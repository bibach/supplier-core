<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.servicerocket.randombits</groupId>
        <artifactId>randombits</artifactId>
        <version>6</version>
        <relativePath />
    </parent>

    <groupId>org.randombits.supplier</groupId>
    <artifactId>supplier-core</artifactId>
    <version>1.2.1</version>

    <name>RB Supplier - Core</name>
    <description>
        The Supplier Plugin provides a simple way for plugins to provide data access points to their own APIs
        that can be used across multiple other plugin, such as the Reporting Plugin, Linking and Scaffolding, for
        example.
    </description>
    <packaging>atlassian-plugin</packaging>
    <url>https://bitbucket.org/servicerocket/supplier-core</url>

    <repositories>
        <repository>
            <id>servicerocket.tools-mvn</id>
            <url>https://t-mvn.performancerocket.com/content/repositories/releases</url>
        </repository>
    </repositories>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/servicerocket/supplier-core.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/servicerocket/supplier-core.git</developerConnection>
        <url>https://bitbucket.org/servicerocket/supplier-core</url>
        <tag>1.2.1</tag>
    </scm>

    <properties>
        <atlassian.plugin.key>org.randombits.supplier.core</atlassian.plugin.key>

        <platform.minVersion>5.3</platform.minVersion>
        <platform.maxVersion>5.9.1-m10</platform.maxVersion>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <!-- Atlassian Dependencies -->
        <atlassian.plugins.core.version>2.9.6</atlassian.plugins.core.version>
        <atlassian.plugins.osgi.version>2.9.6</atlassian.plugins.osgi.version>
        <atlassian.rest.common.version>2.7.3</atlassian.rest.common.version>
        <atlasian.sal.api.version>2.13.0</atlasian.sal.api.version>

        <!-- Customware Dependencies -->
        <support.core.version>[1.3.0,2)</support.core.version>

        <mail.version>1.4</mail.version>
        <slf4j-api.version>1.7.7</slf4j-api.version>
        <joda-time.version>1.6</joda-time.version>
        <!-- Last guava version that support closeQuietly(Closeable closeable) -->
        <guava.version>15.0</guava.version>
        <commons-lang.version>2.6</commons-lang.version>
        <servlet-api.version>2.4</servlet-api.version>

        <spring-beans.version>2.5.2</spring-beans.version>
        <spring-web.version>2.5.2</spring-web.version>
        <jsr311-api.version>1.0</jsr311-api.version>
        <jaxb-api.version>2.1</jaxb-api.version>
        <jsoup.version>1.6.1</jsoup.version>

        <!-- Testing -->
        <randombits.test.version>1</randombits.test.version>
        <jmock-junit4.version>2.5.1</jmock-junit4.version>
        <jmock-legacy.version>2.5.1</jmock-legacy.version>
        <mockito-all.version>1.8.5</mockito-all.version>
        <wink-client.version>1.1.3-incubating</wink-client.version>

        <maven-confluence-plugin.version>3.11</maven-confluence-plugin.version>

        <project.build.sourceRootDirectory>${project.basedir}/src/main</project.build.sourceRootDirectory>
    </properties>

    <dependencies>
        <!-- Confluence Dependencies -->
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence</artifactId>
            <version>${confluence.version}</version>
            <scope>provided</scope>
            <exclusions>
                <exclusion>
                    <groupId>javax.mail</groupId>
                    <artifactId>mail</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>seraph</groupId>
                    <artifactId>seraph</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>opensymphony</groupId>
                    <artifactId>pell-multipart</artifactId>
                </exclusion>
                <exclusion>
                    <artifactId>tangosol</artifactId>
                    <groupId>tangosol-coherence</groupId>
                </exclusion>
                <exclusion>
                    <artifactId>coherence</artifactId>
                    <groupId>tangosol-coherence</groupId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- Bundled -->
        <dependency>
            <groupId>org.randombits.utils</groupId>
            <artifactId>rb-utils</artifactId>
            <version>2.0.0</version>
        </dependency>

        <!-- OSGi -->
        <dependency>
            <groupId>org.randombits.support</groupId>
            <artifactId>support-core</artifactId>
            <version>${support.core.version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- Atlassian -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-core</artifactId>
            <version>${atlassian.plugins.core.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi</artifactId>
            <version>${atlassian.plugins.osgi.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <version>${atlassian.rest.common.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <version>${atlasian.sal.api.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>${guava.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>${commons-lang.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>${joda-time.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.mail</groupId>
            <artifactId>mail</artifactId>
            <version>${mail.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>${servlet-api.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
            <version>${jsr311-api.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>${jaxb-api.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>${spring-beans.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring-web.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j-api.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>${jsoup.version}</version>
        </dependency>

        <!-- Testing -->
        <dependency>
            <groupId>com.servicerocket.randombits</groupId>
            <artifactId>randombits-test</artifactId>
            <version>${randombits.test.version}</version>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>atlassian-confluence-pageobjects</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.upm</groupId>
            <artifactId>atlassian-universal-plugin-manager-pageobjects</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.selenium</groupId>
            <artifactId>atlassian-webdriver-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence-webdriver-support</artifactId>
        </dependency>

        <dependency>
            <groupId>org.jmock</groupId>
            <artifactId>jmock-junit4</artifactId>
            <version>${jmock-junit4.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.jmock</groupId>
            <artifactId>jmock-legacy</artifactId>
            <version>${jmock-legacy.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>${mockito-all.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.wink</groupId>
            <artifactId>wink-client</artifactId>
            <version>${wink-client.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>${project.build.sourceRootDirectory}/resources</directory>
                <excludes>
                    <exclude>atlassian-plugin.xml</exclude>
                </excludes>
            </resource>
            <resource>
                <directory>${project.build.sourceRootDirectory}/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>atlassian-plugin.xml</include>
                </includes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-refapp-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <installPlugin>true</installPlugin>
                    <products>
                        <product>
                            <id>confluence</id>
                            <instanceId>confluence</instanceId>
                            <productDataVersion>${confluence.version}</productDataVersion>
                            <version>${confluence.version}</version>
                        </product>
                    </products>
                    <pluginDependencies>
                        <pluginDependency>
                            <groupId>org.randombits.support</groupId>
                            <artifactId>support-core</artifactId>
                        </pluginDependency>
                    </pluginDependencies>
                    <instructions>
                        <Export-Package>
                            !org.randombits.supplier.core.impl,
                            org.randombits.supplier.core.*;version="${pom.version}"
                        </Export-Package>
                        <Import-Package>
                            org.randombits.support.core.*;version="${support.core.version}",

                            com.atlassian.confluence.*;version="[4,6)",
                            com.atlassian.plugin;version="[2.6,6)",
                            com.atlassian.plugin.*;version="[2.6,6)",
                            com.atlassian.plugins.rest.common.security;version="[2.5,4)",
                            com.atlassian.sal.api.*;version="[2.6,4)",

                            javax.mail.*;version="1.4",
                            javax.servlet.*;version="2.4",
                            javax.ws.rs;version="1.0",
                            javax.ws.rs.core;version="1.0",
                            javax.xml.bind.annotation;version="2.1",

                            org.apache.commons.collections;version="3.2",
                            org.apache.commons.lang*;version="2.4",
                            org.joda.time.*;version="1.6",
                            org.springframework.beans.*;version="2.5",
                            org.slf4j.*;version="1.3",
                        </Import-Package>
                        <Spring-Context>*;timeout:=60</Spring-Context>
                        <CONF_COMM />
                    </instructions>
                    <testGroups>
                        <testGroup>
                            <id>supplier-core-it</id>
                            <productIds>
                                <productId>confluence</productId>
                            </productIds>
                            <includes>
                                <include>it/**/*Test.java</include>
                            </includes>
                        </testGroup>
                    </testGroups>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <compilerArgument>-proc:none</compilerArgument>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-release-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <!-- Required for deployment to Sonatype -->
            <id>release-sign-artifacts</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
