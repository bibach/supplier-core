Supplier Core
=============

2015-11-16, 1.2.1
-----------------
 * Add missing Map Supplier to plugin descriptor

2015-10-28, 1.2.0
-----------------
 * WIKI-536 Remove duplicated user supplier.

2015-10-06, 1.1.0
-----------------
 * WIKI-488 Compatible with Confluence 5.9.

2014-12-16, 1.0.10
-----------------
 * WIKI-126 Change the return type of TextSupplier on method splitWith().