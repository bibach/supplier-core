package org.randombits.supplier.core;

import java.util.Comparator;

/**
 * Key handlers do the interpretation of individual key values (eg 'foo:bar') and return a result. The
 * {@link #process(SupplierContext, String)} method does the actual execution. The other methods provide
 * details about the supplier, mostly for informational purposes, however the {@link #getKeyPattern()} method returns
 * the details about how the key is parsed.
 */
public interface KeyHandler {

    /**
     * Sorts key details by weight.
     */
    public static final Comparator<KeyHandler> COMPARATOR = new Comparator<KeyHandler>() {
        @Override
        public int compare( KeyHandler keyHandler1, KeyHandler keyHandler2 ) {
            return keyHandler2.getWeight() - keyHandler1.getWeight();
        }
    };

    /**
     * @return the Supplier this handler is associated with.
     */
    Supplier getSupplier();

    /**
     * @return the I18N key for the supplier key label, for documentation.
     */
    String getLabelKey();

    /**
     * @return the I18N key for the supplier description, for documentation.
     */
    String getDescriptionKey();

    /**
     * @return the {@link KeyPattern} that this handler will match.
     */
    KeyPattern getKeyPattern();

    /**
     * @return The Class that the {@link SupplierContext#getValue()} must be
     *         an instance of to be executed by this handler. May be <code>null</code> if any class, or even a
     *         <code>null</code> value, is supported.
     */
    Class<?> getValueType();

    /**
     * @return the type of object that will be returned by this handler.
     */
    Class<?> getReturnType();

    /**
     * The weight of this handler, relative to others in the same Supplier. A higher weight gives the handler
     * a higher priority.
     *
     * @return The weight;
     */
    int getWeight();

    /**
     * Processes the key value and returns the result, based on the current value in the SupplierContext.
     *
     * @param context  the context to process against.
     * @param keyValue The key value to process.
     * @return the result.
     * @throws SupplierException if there is a problem during processing.
     */
    Object process( SupplierContext context, String keyValue ) throws SupplierException;
}
