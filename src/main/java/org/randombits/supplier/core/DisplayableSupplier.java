package org.randombits.supplier.core;

import org.randombits.utils.lang.API;

/**
 * Suppliers which implement this interface are providing a 'display' text value
 * for a given value. This is useful for more complex objects which do not
 * have a 'human-readable' 'toString()' method result.
 */
@API("1.0.0")
public interface DisplayableSupplier extends Supplier {

    @API("1.0.0")
    public static final Property<String> DISPLAY = new Property<String>() {
        @Override
        public String get( Supplier supplier, SupplierContext options ) throws SupplierException {
            if ( supplier instanceof DisplayableSupplier ) {
                return ( (DisplayableSupplier) supplier ).getDisplayText( options );
            }
            return null;
        }
    };

    @API("1.0.0")
    String getDisplayText( SupplierContext context ) throws SupplierException;
}
