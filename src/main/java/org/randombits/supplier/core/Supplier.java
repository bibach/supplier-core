package org.randombits.supplier.core;

import org.randombits.utils.lang.API;

import java.util.Collection;
import java.util.Set;

/**
 * Implementers of this interface act as a middle-man for
 * accessing data from a given context.
 */
@API("1.0.0")
public interface Supplier {

    /**
     * Returns the list of class types supported by this Supplier.
     * If the supplier supports all types, it should return <code>null</code>.
     *
     * @return The list of supported class types.
     */
    @API("1.0.0")
    Set<Class<?>> getSupportedTypes();

    /**
     * @return <code>true</code> if the supplier supports receiving a <code>null</code> context value.
     */
    @API("1.0.0")
    boolean isNullAllowed();

    /**
     * Returns the prefix this supplier supports.
     *
     * @return The prefix.
     */
    @API("1.0.0")
    Set<String> getPrefixes();

    /**
     * In some cases, the supported type may be sufficient to identify
     * a value, but some suppliers may wish to only get executed if the
     * prefix is explicitly provided. This prevents more expensive suppliers
     * from getting processed unnecessarily.
     *
     * @return <code>false</code> if the prefix is optional.
     */
    @API("1.0.0")
    boolean isPrefixRequired();

    @API("1.0.0")
    Collection<? extends KeyHandler> getKeyDetails();

    @API("1.0.0")
    public interface Property<T> {

        @API("1.0.0")
        T get( Supplier supplier, SupplierContext context ) throws SupplierException;
    }
}
