package org.randombits.supplier.core.special;

import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.AnnotatedSupplier;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.utils.lang.API;

/**
 * Always returns the same value if the key is '@self'. Otherwise, returns null.
 */
@API("1.0.0")
public class SelfSupplier extends AnnotatedSupplier {

    /**
     * Constructs an annotated supplier.
     */
    public SelfSupplier() {
        super((String) null, true, new Class<?>[]{Object.class}, false);
    }

    @SupplierKey("@self")
    @API("1.0.0")
    public Object getValue(@KeyValue Object value) throws SupplierException {
        return value;
    }
}
