package org.randombits.supplier.core;

import org.randombits.utils.lang.API;

/**
 * The Supplier Assistant makes looking up and parsing supplier keychain requests
 * simple. It also serves as the central repository for all registered Suppliers
 * in the system.
 */
@API("1.0.0")
public interface SupplierAssistant {

    /**
     * The delimiter used to escape supplier key chains passed to
     * {@link #injectValues(Object, String)} ('%').
     */
    char ESCAPE_DELIM = '%';

    /**
     * @return The list of current installed and enabled {@link Supplier}s.
     */
    Iterable<Supplier> getSuppliers();

    /**
     * @return The list of {@link SupplierDetails} values for the current set of installed suppliers.
     */
    Iterable<SupplierDetails> getSupplierDetails();

    /**
     * @param prefix The prefix.
     * @return The list of {@link SupplierDetails} values that support the specified prefix.
     */
    Iterable<SupplierDetails> getSupplierDetailsForPrefix( String prefix );

    /**
     * Creates a {@link SupplierContext} instance, initialised to the current details of
     * the current session, including user timezone and locale. This creates a new instance
     * each time it is called, and is used for the <code>injectValues</code> and <code>getValue</code>
     * methods that do have an 'options' parameter.
     *
     * @param value The value to create a context with.
     * @return The supplier options.
     */
    @API("1.0.0")
    SupplierContext createContext( Object value );

    /**
     * This method will replace '%prefix:key%' sequences with the value of the
     * specified keychain given the specified context. If the whole text value
     * is a single keychain surrounded by '%' characters, the actual object
     * being referred to is returned. This provides a way of accessing actual
     * objects from text parameters.
     * <p>For example:</p>
     * <pre>SupplierAssistant.getInstance().processValues( &quot;Foo&quot;, &quot;'%@self%' is %text:length% characters long.&quot; );</pre>
     * <p/>This will result in displaying the string:<p/>
     * <pre>'Foo' is 3 characters long.</pre>
     * <p/>
     * <p>The value between '%'s can be full key chains. Eg:</p>
     * <pre>&quot;Foo: value='%map:foo%'; length=%map:foo &gt; text:length%&quot;</pre>
     * <p/>
     * <p>To output a '%' character, escape it by putting '%%'. Eg:</p>
     * <pre>* &quot;%data:Growth%%%&quot;</pre>
     * <p/>
     * <p>If the value of 'data:Growth' is 20, the output will be:</p>
     * <pre>&quot;20%&quot;</pre>
     *
     * @param value The context object.
     * @param text  The text containing '%prefix:key%' values.
     * @return Either the single injected object, or a string with the %values%
     *         injected.
     * @throws SupplierException if there is a problem processing the injection.
     */
    @API("1.0.0")
    Object injectValues( Object value, String text ) throws SupplierException;

    /**
     * This method will replace '%prefix:key%' sequences with the value of the
     * specified keychain given the specified context. If the whole text value
     * is a single keychain surrounded by '%' characters, the actual object
     * being referred to is returned. This provides a way of accessing actual
     * objects from text parameters.
     * <p>For example:</p>
     * <pre>SupplierAssistant.getInstance().processValues( &quot;Foo&quot;, &quot;'%@self%' is %text:length% characters long.&quot; );</pre>
     * <p/>This will result in displaying the string:<p/>
     * <pre>'Foo' is 3 characters long.</pre>
     * <p/>
     * <p>The value between '%'s can be full key chains. Eg:</p>
     * <pre>&quot;Foo: value='%map:foo%'; length=%map:foo &gt; text:length%&quot;</pre>
     * <p/>
     * <p>To output a '%' character, escape it by putting '%%'. Eg:</p>
     * <pre>* &quot;%data:Growth%%%&quot;</pre>
     * <p/>
     * <p>If the value of 'data:Growth' is 20, the output will be:</p>
     * <pre>&quot;20%&quot;</pre>
     *
     * @param value   The context object.
     * @param text    The text containing '%prefix:key%' values.
     * @param encoder If provided, the encoder will decode the contents of the keychain before processing, then encode the results.
     * @return Either the single injected object, or a string with the %values%
     *         injected.
     * @throws SupplierException if there is a problem processing the injection.
     */
    @API("1.0.0")
    Object injectValues( Object value, String text, Encoder encoder ) throws SupplierException;

    /**
     * This method will replace '%prefix:key%' sequences with the value of the
     * specified keychain given the specified context. If the whole text value
     * is a single keychain surrounded by '%' characters, the actual object
     * being referred to is returned. This provides a way of accessing actual
     * objects from text parameters.
     * <p>For example:</p>
     * <pre>SupplierAssistant.getInstance().processValues( &quot;Foo&quot;, &quot;'%@self%' is %text:length% characters long.&quot; );</pre>
     * <p/>This will result in displaying the string:<p/>
     * <pre>'Foo' is 3 characters long.</pre>
     * <p/>
     * <p>The value between '%'s can be full key chains. Eg:</p>
     * <pre>&quot;Foo: value='%map:foo%'; length=%map:foo &gt; text:length%&quot;</pre>
     * <p/>
     * <p>To output a '%' character, escape it by putting '%%'. Eg:</p>
     * <pre>* &quot;%data:Growth%%%&quot;</pre>
     * <p/>
     * <p>If the value of 'data:Growth' is 20, the output will be:</p>
     * <pre>&quot;20%&quot;</pre>
     *
     * @param context The context to use when processing the request.
     * @param text    The text containing '%prefix:key%' values.
     * @return Either the single injected object, or a string with the %values%
     *         injected.
     * @throws SupplierException if there is a problem processing the injection.
     */
    @API("1.0.0")
    Object injectValues( SupplierContext context, String text ) throws SupplierException;

    /**
     * This method will replace '%prefix:key%' sequences with the value of the
     * specified keychain given the specified context. If the whole text value
     * is a single keychain surrounded by '%' characters, the actual object
     * being referred to is returned. This provides a way of accessing actual
     * objects from text parameters.
     * <p>For example:</p>
     * <pre>SupplierAssistant.getInstance().processValues( &quot;Foo&quot;, &quot;'%@self%' is %text:length% characters long.&quot; );</pre>
     * <p/>This will result in displaying the string:<p/>
     * <pre>'Foo' is 3 characters long.</pre>
     * <p/>
     * <p>The value between '%'s can be full key chains. Eg:</p>
     * <pre>&quot;Foo: value='%map:foo%'; length=%map:foo &gt; text:length%&quot;</pre>
     * <p/>
     * <p>To output a '%' character, escape it by putting '%%'. Eg:</p>
     * <pre>* &quot;%data:Growth%%%&quot;</pre>
     * <p/>
     * <p>If the value of 'data:Growth' is 20, the output will be:</p>
     * <pre>&quot;20%&quot;</pre>
     *
     * @param context The context to use when processing the request.
     * @param text    The text containing '%prefix:key%' values.
     * @param encoder If provided, the encoder will decode the contents of the keychain before processing, then encode the results.
     * @return Either the single injected object, or a string with the %values%
     *         injected.
     * @throws SupplierException if there is a problem processing the injection.
     */
    @API("1.0.0")
    Object injectValues( SupplierContext context, String text, Encoder encoder ) throws SupplierException;

    /**
     * Attempts to find a value given the specified context and key. String
     * values are <b>not</b> rendered as wiki text. Uses {@link #createContext(Object)}
     * to create the supplier context.
     *
     * @param value    The context object.
     * @param keyChain The key list, separated by '>' characters.
     * @return the found value, or <code>defaultValue</code> if none could be
     *         found.
     * @throws SupplierException if there is a problem finding the value.
     */
    @API("1.0.0")
    Object findValue( Object value, String keyChain ) throws SupplierException;

    /**
     * Attempts to find a value given the specified context and key. String
     * values are <b>not</b> rendered as wiki text.
     *
     * @param context  The context to use when processing the request.
     * @param keyChain The key list, separated by '>' characters.
     * @return the found value, or <code>defaultValue</code> if none could be
     *         found.
     * @throws SupplierException if there is a problem finding the value.
     */
    @API("1.0.0")
    Object findValue( SupplierContext context, String keyChain ) throws SupplierException;

    @API("1.0.0")
    <T> T findProperty( Object value, String keyChain, Supplier.Property<T> property ) throws SupplierException;

    @API("1.0.0")
    <T> T findProperty( SupplierContext context, String keyChain, Supplier.Property<T> property )
            throws SupplierException;
}
