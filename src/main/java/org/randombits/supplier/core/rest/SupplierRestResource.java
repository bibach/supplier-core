package org.randombits.supplier.core.rest;

import org.randombits.supplier.core.SupplierAssistant;
import org.randombits.supplier.core.SupplierDetails;
import org.randombits.supplier.core.SupplierException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Provides REST access to Supplier queries.
 */
@Path("/")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class SupplierRestResource {

    private final SupplierAssistant supplierAssistant;

    private static final String NULL_PREFIX = "@null";

    public SupplierRestResource( SupplierAssistant supplierAssistant ) {
        this.supplierAssistant = supplierAssistant;
    }

    /**
     * Evaluates the provided keychain. There is no context object provided, so the first key should create
     * or find an object independently.
     *
     * @param keychain The keychain to evaluate.
     * @return The result of the evaluation.
     */
    @GET
    @Path("eval")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getValue( @QueryParam( "keychain" ) String keychain ) {
        Object value = "";
        try {
            Object result = supplierAssistant.findValue( value, keychain );
            return Response.ok( result ).build();
        } catch ( SupplierException e ) {
            e.printStackTrace();
            return Response.status( Response.Status.INTERNAL_SERVER_ERROR ).build();
        }
    }

    /**
     * Gets the full list of currently installed an enabled supplier.
     *
     * @return The full Supplier list.
     */
    @GET
    @Path("suppliers")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getSuppliers() {
        Iterable<SupplierDetails> suppliers = supplierAssistant.getSupplierDetails();
        return Response.ok( suppliers ).build();
    }

    /**
     * Finds the list of suppliers that have the specified prefix. Often it will just be one, but it is possible
     * for multiple suppliers to use the same prefix.
     *
     * @param prefix The prefix to search for.
     * @return The list of Suppliers using that prefix.
     */
    @GET
    @Path("suppliers/prefix/{prefix}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getSuppliersForPrefix( @PathParam( "prefix" ) @DefaultValue( NULL_PREFIX ) String prefix ) {
        prefix = NULL_PREFIX.equals( prefix ) ? null : prefix;

        Iterable<SupplierDetails> suppliers = supplierAssistant.getSupplierDetailsForPrefix( prefix );
        return Response.ok( suppliers ).build();
    }
}