package org.randombits.supplier.core.annotate.apt;

import com.google.common.base.Joiner;
import org.randombits.supplier.core.KeyParseException;
import org.randombits.supplier.core.KeyPattern;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.lang.model.util.ElementScanner6;
import javax.tools.Diagnostic;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Checks that methods using the {@link org.randombits.supplier.core.annotate.SupplierKey},
 * {@link org.randombits.supplier.core.annotate.KeyContext}, {@link org.randombits.supplier.core.annotate.KeyValue},
 * {@link org.randombits.supplier.core.annotate.KeyParam} and
 * {@link org.randombits.supplier.core.annotate.KeyWeight} annotations are following the rules.
 */
@SupportedAnnotationTypes("net.customware.supplier.annotate.*")
@API("1.0.0")
public class SupplierKeyProcessor extends AbstractProcessor {

    private SupplierKeyChecker supplierKeyChecker;

    @Override
    public SourceVersion getSupportedSourceVersion() {
        /*
         * Return latest source version instead of a fixed version
         * like RELEASE_6.  To return a fixed version, this class
         * could be annotated with a SupportedSourceVersion
         * annotation.
         *
         * Warnings will be issued if any unknown language constructs
         * are encountered.
         */
        return SourceVersion.latest();
    }

    @Override
    public boolean process( Set<? extends TypeElement> typeElements, RoundEnvironment round ) {
        processingEnv.getMessager().printMessage( Diagnostic.Kind.NOTE,
                "Processing Supplier Annotations." );

        if ( !round.processingOver() ) {
            for ( Element element : round.getRootElements() )
                supplierKeyChecker.checkElement( element );
        }

//        processingEnv.getMessager().printMessage( Diagnostic.Kind.NOTE,
//                "Completed processing Supplier Annotations" );

        return false;
    }


    @Override
    public void init( ProcessingEnvironment processingEnv ) {
        super.init( processingEnv );
        supplierKeyChecker = new SupplierKeyChecker( processingEnv );
    }

    private static class SupplierKeyChecker {

        ProcessingEnvironment processingEnv;

        SupplierKeyScanner scanner = new SupplierKeyScanner();

        private SupplierKeyChecker( ProcessingEnvironment processingEnvironment ) {
            this.processingEnv = processingEnvironment;
        }

        public void checkElement( Element element ) {
            scanner.scan( element, false );
        }

        private void printMessage( Diagnostic.Kind kind, String message ) {
            processingEnv.getMessager().printMessage( kind, message );
        }

        private void printMessage( Diagnostic.Kind kind, String message, Element element ) {
            processingEnv.getMessager().printMessage( kind, message, element );
        }

        private void printMessage( Diagnostic.Kind kind, String message, Element element, AnnotationMirror annotationMirror ) {
            processingEnv.getMessager().printMessage( kind, message, element, annotationMirror );
        }

        private void printMessage( Diagnostic.Kind kind, String message, Element element, AnnotationMirror annotationMirror, AnnotationValue value ) {
            processingEnv.getMessager().printMessage( kind, message, element, annotationMirror, value );
        }

        private class SupplierKeyScanner extends ElementScanner6<Void, Boolean> {

            @Override
            public Void visitExecutable( ExecutableElement executable, Boolean doCheck ) {
//                printMessage( Diagnostic.Kind.NOTE,
//                        "Started processing Executable '" + executable + "'" );

                List<? extends AnnotationMirror> annotations = executable.getAnnotationMirrors();

                for ( AnnotationMirror annotation : annotations ) {
                    String annotationType = annotation.getAnnotationType().toString();
                    if ( SupplierKey.class.getName().equals( annotationType ) ) {
                        doCheck = true;
                        checkSupplierKey( executable, annotation );
                    } else if ( KeyWeight.class.getName().equals( annotationType ) ) {
                        checkWeight( executable, annotation );
                    }
                }

//                printMessage( Diagnostic.Kind.NOTE,
//                        "Completed processing Element '" + executable + "'" );

                super.visitExecutable( executable, doCheck );

                return null;
            }

            private void checkWeight( ExecutableElement executable, AnnotationMirror annotation ) {
                SupplierKey supplierKey = executable.getAnnotation( SupplierKey.class );
                if ( supplierKey == null )
                    printMessage( Diagnostic.Kind.WARNING,
                            name( KeyWeight.class ) + " annotations are usually applied to methods with a "
                                    + name( SupplierKey.class ) + " annotation.",
                            executable, annotation );
            }


            /**
             * Checks that the provided element follows the rules for SupplierKey methods.
             *
             * @param method     The method to check.
             * @param annotation The \@SupplierKey annotation.
             */
            private void checkSupplierKey( ExecutableElement method, AnnotationMirror annotation ) {
                // Get the key pattern values
                AnnotationValue value = getAnnotationValue( annotation, "value" );

                if ( value == null || value.getValue() == null ) {
                    printMessage( Diagnostic.Kind.ERROR,
                            name( SupplierKey.class ) + " must have at least one pattern.", method, annotation, value );
                } else if ( value.getValue() instanceof List ) {
                    @SuppressWarnings({"unchecked"})
                    List<? extends AnnotationValue> patterns = (List<? extends AnnotationValue>) value.getValue();
                    if ( patterns == null || patterns.size() == 0 ) {
                        printMessage( Diagnostic.Kind.ERROR,
                                name( SupplierKey.class ) + " must have at least one pattern.", method, annotation, value );
                    } else {
                        Set<String> methodParams = new HashSet<String>();

                        for ( VariableElement parameter : method.getParameters() ) {
                            KeyParam keyParam = parameter.getAnnotation( KeyParam.class );
                            KeyValue keyValue = parameter.getAnnotation( KeyValue.class );
                            KeyContext keyContext = parameter.getAnnotation( KeyContext.class );

                            if ( keyParam != null )
                                methodParams.add( keyParam.value() );

                            if ( keyParam == null && keyValue == null && keyContext == null )
                                printMessage( Diagnostic.Kind.ERROR,
                                        "The '" + parameter.getSimpleName() + "' parameter must be annotated with @KeyParam, @KeyValue or @KeyContext",
                                        parameter );

                        }

                        // Check that all parameters in all patterns are accounted for.
                        for ( AnnotationValue pattern : patterns ) {
                            checkSupplierKeyPattern( method, annotation, value, (String) pattern.getValue(), methodParams );
                        }
                    }
                }
            }

            private void checkSupplierKeyPattern( ExecutableElement method, AnnotationMirror annotation, AnnotationValue value, String pattern, Set<String> methodParams ) {
                try {
                    // Check the key parses.
                    KeyPattern keyPattern = KeyPattern.parse( pattern );
                    // Now, check that all named parameters have an @KeyParam in the method parameter list.
                    Set<String> keyParams = new HashSet<String>( keyPattern.getParameters() );
//                    printMessage( Diagnostic.Kind.NOTE,
//                            "The '" + method + "' has the following pattern parameters: " + Joiner.on( ", " ).join( keyParams ) );
//                    printMessage( Diagnostic.Kind.NOTE,
//                            "The '" + method + "' has the following @KeyParam values: " + Joiner.on( ", " ).join( methodParams ) );

                    keyParams.removeAll( methodParams );
                    if ( !keyParams.isEmpty() ) {
                        printMessage( Diagnostic.Kind.ERROR,
                                "The '" + method.toString() + "' method must have parameters annotated with @KeyParam for the following: \""
                                        + Joiner.on( "\", \"" ).join( keyParams ) + "\"", method, annotation, value );
                    }
                } catch ( KeyParseException e ) {
                    printMessage( Diagnostic.Kind.ERROR, e.getMessage(), method, annotation, value );
                }
            }

            @Override
            public Void visitVariable( VariableElement variable, Boolean doCheck ) {
//                printMessage( Diagnostic.Kind.NOTE, "Visiting variable: " + variable );
                if ( variable.getKind() == ElementKind.PARAMETER ) {
                    int count = 0;
                    // Check for KeyContext params
                    KeyContext context = variable.getAnnotation( KeyContext.class );
                    if ( context != null ) {
                        checkKeyContext( variable );
                        count++;
                    }

                    KeyParam param = variable.getAnnotation( KeyParam.class );
                    if ( param != null ) {
                        checkKeyParam( variable, param );
                        count++;
                    }

                    if ( variable.getAnnotation( KeyValue.class ) != null ) {
                        count++;
                    }

                    if ( count > 1 )
                        printMessage( Diagnostic.Kind.ERROR,
                                "Only one of either @KeyContext, @KeyValue or @KeyParam should be on any given parameter at one time.",
                                variable );

                    if ( doCheck && count == 0 )
                        printMessage( Diagnostic.Kind.ERROR,
                                "The '" + variable.getSimpleName() + "' parameter must be annotated with @KeyParam, @KeyValue or @KeyContext",
                                variable );

                }

                return null;
            }

            private void checkKeyParam( VariableElement variable, KeyParam param ) {
                // Check the type is a String
                if ( !String.class.getName().equals( variable.asType().toString() ) )
                    printMessage( Diagnostic.Kind.ERROR,
                            "The '" + variable.getSimpleName() + "' parameter annotated with @KeyParam is required to be a String.", variable );

                // Check if the named parameter exists in the @SupplierKey
                SupplierKey key = variable.getEnclosingElement().getAnnotation( SupplierKey.class );
                if ( key == null ) {
                    printMessage( Diagnostic.Kind.WARNING,
                            "The '" + variable.getSimpleName() + "' parameter is annotated with @KeyParam, but there is no @SupplierKey for the method." );
                } else {
                    boolean found = false;
                    for ( String pattern : key.value() ) {
                        try {
                            KeyPattern keyPatternPattern = KeyPattern.parse( pattern );
                            if ( keyPatternPattern.getParameters().contains( param.value() ) )
                                found = true;
                        } catch ( KeyParseException e ) {
                            // Do nothing - reporting parser errors is not this check's job.
                        }
                    }
                    if ( !found )
                        printMessage( Diagnostic.Kind.WARNING,
                                "The parameter is marked as @KeyParam(\"" + param.value() + "\"), but there is no matching \"{" + param.value() + "}\" in any of the patterns in the @SupplierKey for this method.",
                                variable );
                }
            }

            private void checkKeyContext( VariableElement variableElement ) {
                if ( !SupplierContext.class.getName().equals( variableElement.asType().toString() ) )
                    printMessage( Diagnostic.Kind.ERROR,
                            "Parameters marked with @KeyContext must be an instance of " + name( SupplierContext.class ), variableElement );
            }


            private String name( Class<?> annotationType ) {
                return ( annotationType.isAnnotation() ? "@" : "" ) + annotationType.getName();
            }

            private AnnotationValue getAnnotationValue( AnnotationMirror annotation, String name ) {
                Map<? extends ExecutableElement, ? extends AnnotationValue> properties = annotation.getElementValues();
                for ( Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> e : properties.entrySet() ) {
                    if ( e.getKey().getSimpleName().contentEquals( name ) )
                        return e.getValue();
                }
                return null;
            }
        }

    }
}
