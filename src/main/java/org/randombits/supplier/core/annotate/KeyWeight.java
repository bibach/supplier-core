package org.randombits.supplier.core.annotate;

import java.lang.annotation.*;

/**
 * Allows a 'key' method to be given a weight value. The default
 * weight of any key method is 0. To give a particular key precedence,
 * give it a higher weight. To push it to the back, give it a negative weight.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface KeyWeight {

    /**
     * @return the weight value.
     */
    int value();
}
