package org.randombits.supplier.core.annotate;

import com.google.common.base.Joiner;
import org.randombits.supplier.core.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Represents an annotated method that is a key handler. Methods must have the following:
 * <p/>
 * <ol>
 * <li>Be annotated with @SupplierKey with one or more strings for the key.</li>
 * <li>Have the first parameter of the method being a SupplierContext value.</li>
 * <li>Have a matching String parameter annotated with @KeyParam("param") for each "{param}" in all @SupplierKey string values.</li>
 * </ol>
 * <p/>
 * Eg:
 * <p/>
 * <pre>
 * \@KeyParam("foo {bar}")
 * public MyObject doFoo( SupplierContext context, @KeyParam("bar") String myBar ) throws SupplierException {
 *     ...
 * }
 * </pre>
 */
class AnnotatedKeyHandler implements KeyHandler {

    private final AnnotatedSupplier supplier;

    private final KeyPattern keyPattern;

    private final Method method;

    private final Annotation[] paramKinds;

    private Class<?> valueType;

    private final int weight;

    public AnnotatedKeyHandler( KeyPattern keyPattern, Method method, AnnotatedSupplier supplier ) throws AnnotatedKeyDetailsException {
        this.method = method;
        this.supplier = supplier;
        this.keyPattern = keyPattern;

        // Make the method accessible
        try {
            method.setAccessible( true );
        } catch ( SecurityException e ) {
            throw new AnnotatedKeyDetailsException( "Unable to make method accessible: " + method );
        }

        // Check that the method is the right kind.
        Class<?>[] paramTypes = method.getParameterTypes();
        this.paramKinds = new Annotation[paramTypes.length];

        Annotation[][] paramAnnotations = method.getParameterAnnotations();

        // Initialise the parameter names.
        Set<String> unusedParams = new HashSet<String>( keyPattern.getParameters() );
        // Default to 'any' value type.
        valueType = null;

        for ( int i = 0; i < paramTypes.length; i++ ) {

            Annotation[] annotations = paramAnnotations[i];
            for ( Annotation annotation : annotations ) {
                if ( annotation instanceof KeyParam || annotation instanceof KeyValue || annotation instanceof KeyContext ) {
                    paramKinds[i] = annotation;
                    break;
                }
            }

            if ( paramKinds[i] == null ) {
                throw new AnnotatedKeyDetailsException( getParamDetails( method, i ) + " must be annotated with @KeyParam, @KeyValue or @KeyContext" );
            } else if ( paramKinds[i] instanceof KeyParam ) {
                if ( !paramTypes[i].equals( String.class ) ) {
                    throw new AnnotatedKeyDetailsException( getParamDetails( method, i ) + " must be a String value to be a @KeyParam." );
                } else {
                    // Clear the name from the unused set
                    unusedParams.remove( ( (KeyParam) paramKinds[i] ).value() );
                }
            } else if ( paramKinds[i] instanceof KeyContext && !SupplierContext.class.equals( paramTypes[i] ) ) {
                throw new AnnotatedKeyDetailsException( getParamDetails( method, i ) + " must be a SupplierContext to be a @KeyContext." );
            } else if ( paramKinds[i] instanceof KeyValue ) {
                if ( valueType != null )
                    throw new AnnotatedKeyDetailsException( getParamDetails( method, i ) + " cannot be annotated @KeyValue because another parameter is already the @KeyValue." );
                Set<Class<?>> supportedTypes = supplier.getSupportedTypes();
                if ( supportedTypes == null ) {
                    if ( !Object.class.equals( paramTypes[i] ) ) {
                        throw new AnnotatedKeyDetailsException( getParamDetails( method, i ) + " must be an Object in order to a valid @KeyValue for this Supplier." );
                    }
                } else {
                    // Check that the target value is compatible with at least one supported type
                    boolean compatible = false;
                    for ( Class<?> supportedType : supportedTypes ) {
                        if ( paramTypes[i].isAssignableFrom( supportedType ) ) {
                            compatible = true;
                            break;
                        }
                    }
                    if ( !compatible )
                        throw new AnnotatedKeyDetailsException( getParamDetails( method, i ) + " must be a compatible with at least one of the supported types for the Supplier: " + paramTypes[i].getName() );
                }
                valueType = paramTypes[i];
            }
        }

        if ( unusedParams.size() > 0 )
            throw new AnnotatedKeyDetailsException( unusedParams.size() + " parameters named in the key do not have matching @KeyParam method parameters: "
                    + Joiner.on( ", " ).join( unusedParams ) );

        // Set the weight.
        KeyWeight weight = method.getAnnotation( KeyWeight.class );
        this.weight = weight != null ? weight.value() : 0;
    }

    private String getParamDetails( Method method, int i ) {
        return "Parameter #" + ( i + 1 ) + " of the '" + method.getDeclaringClass().getName() + "#" + method.getName() + "' method";
    }

    public Supplier getSupplier() {
        return supplier;
    }

    private String getI18NKey( String name ) {
        return method.getDeclaringClass().getName() + "." + method.getName() + "." + name;
    }

    /**
     * Returns the I18N key for the name of this Key. It is
     * a combination of the Supplier's class name + "." + method name + ".label". Eg:
     * <p/>
     * <code>package.Class.methodName.label</code>
     *
     * @return The I18N key for the name.
     */
    @Override
    public String getLabelKey() {
        return getI18NKey( "label" );
    }

    @Override
    public String getDescriptionKey() {
        return getI18NKey( "desc" );
    }

    @Override
    public KeyPattern getKeyPattern() {
        return keyPattern;
    }

    public Class<?> getValueType() {
        return valueType;
    }

    @Override
    public Class<?> getReturnType() {
        return method.getReturnType();
    }

    @Override
    public Object process( SupplierContext context, String keyValue ) throws SupplierException {
        Class<?> targetValueType = findTargetValueType( context );
        if ( targetValueType == null && valueType != null )
            return null;

        Map<String, String> params = new HashMap<String, String>();
        if ( keyPattern.process( keyValue, params ) == KeyPattern.Status.COMPLETE ) {
            Object[] methodParams = new Object[paramKinds.length];
            // Pull the matching parameters from the map.
            for ( int i = 0; i < paramKinds.length; i++ ) {
                Annotation kind = paramKinds[i];
                if ( kind instanceof KeyValue ) {
                    methodParams[i] = supplier.getValueAs( context, targetValueType );
                    // Check that the value is actually supported by this supplier.
                    if ( !supplier.checkSupportedValue( methodParams[i] ) )
                        return null;
                } else if ( kind instanceof KeyContext ) {
                    methodParams[i] = context;
                } else if ( kind instanceof KeyParam ) {
                    methodParams[i] = params.get( ( (KeyParam) kind ).value() );
                }
            }

            try {
                return method.invoke( supplier, methodParams );
            } catch ( IllegalAccessException e ) {
                throw new SupplierException( "Unable to execute method: " + method, e );
            } catch ( InvocationTargetException e ) {
                if ( e.getCause() instanceof SupplierException )
                    throw (SupplierException) e.getCause();
                throw new SupplierException( e.getCause().getMessage(), e.getCause() );
            }
        }
        return null;
    }

    private Class<?> findTargetValueType( SupplierContext context ) {
        if ( valueType == null )
            return null;

        Set<Class<?>> supportedTypes = supplier.getSupportedTypes();

        if ( supportedTypes == null )
            return valueType;

        for ( Class<?> type : supportedTypes ) {
            if ( valueType.isAssignableFrom( type ) && supplier.canGetValueAs( context, type ) )
                return type;
        }

        return null;
    }

    public int getWeight() {
        return weight;
    }

    public String toString() {
        return keyPattern.toString();
    }
}
