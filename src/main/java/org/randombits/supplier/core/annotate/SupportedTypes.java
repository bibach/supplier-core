package org.randombits.supplier.core.annotate;

import java.lang.annotation.*;

/**
 * Marks an {@link AnnotatedSupplier} as having particular class types that it supports.
 * This annotation is optional. If it is not provided, the supplier will be passed anything
 * and everything. If it is provided, it will only be asked to handle classes of the specified types.
 *
 * @see AnnotatedSupplier
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SupportedTypes {

    /**
     * The set of {@link Class} types that this supplier supports. If any type is supported,
     * the value should be an empty array. (\{})
     *
     * @return The prefix values.
     */
    Class<?>[] value();

    /**
     * If <code>false</code>, the prefix will not inherit any {@link SupportedTypes} settings
     * from superclasses. The default is <code>true</code>.
     *
     * @return <code>true</code> if prefixes should be inherited.
     */
    boolean inherit() default true;

    /**
     * If <code>true</code>, the supplier will allow a null value in addition to any specified classes in
     * {@link #value()}. Defaults to <code>false</code>.
     *
     * @return <code>true</code> if null values are allowed.
     */
    boolean nullAllowed() default false;
}
