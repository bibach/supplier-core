package org.randombits.supplier.core.annotate;

import java.lang.annotation.*;

/**
 * Marks a parameter as being a particular named parameter from the {@link SupplierKey}.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface KeyParam {

    /**
     * The name of the parameter. Should match a named parameter in the {@link SupplierKey} annotation for the same method.
     *
     * @return The parameter name.
     */
    String value();
}
