package org.randombits.supplier.core.annotate;

import java.lang.annotation.*;

/**
 * Marks a parameter as being the value that is being operated on.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface KeyValue {
}
