package org.randombits.supplier.core.annotate;

/**
 * This exception is thrown when an annotated key handler method has a bad
 * configuration. It should never happen for correctly configured Suppliers.
 */
public class AnnotatedKeyDetailsException extends RuntimeException {

    public AnnotatedKeyDetailsException() {
    }

    public AnnotatedKeyDetailsException( String s ) {
        super( s );
    }

    public AnnotatedKeyDetailsException( String s, Throwable throwable ) {
        super( s, throwable );
    }

    public AnnotatedKeyDetailsException( Throwable throwable ) {
        super( throwable );
    }
}
