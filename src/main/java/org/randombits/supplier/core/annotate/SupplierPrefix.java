package org.randombits.supplier.core.annotate;

import java.lang.annotation.*;

/**
 * Specifies a prefix for an {@link AnnotatedSupplier}. If a subclass of AnnotatedSupplier is constructed
 * with the default (no argument) constructor, it must have an \@SupplierPrefix annotation to provide
 * at least one prefix value.
 *
 * @see org.randombits.supplier.core.annotate.AnnotatedSupplier
 * @see SupportedTypes
 * @see SupplierKey
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SupplierPrefix {

    /**
     * The set of prefixes that this supplier will match.
     *
     * @return The prefix values.
     */
    String[] value();

    /**
     * If <code>true</code>, the prefix <b>must</b> be supplied. Otherwise, it's optional.
     *
     * @return <code>true</code> if the prefix is required.
     */
    boolean required() default false;

    /**
     * If <code>false</code>, the prefix will not inherit any {@link SupplierPrefix} settings
     * from superclasses. The default is <code>true</code>.
     *
     * @return <code>true</code> if prefixes should be inherited.
     */
    boolean inherit() default true;
}
