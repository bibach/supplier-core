package org.randombits.supplier.core;

import org.randombits.support.core.convert.ConversionAssistant;
import org.randombits.support.core.convert.ConversionException;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * A default implementation of {@link org.randombits.supplier.core.SupplierContext}.
 */
public class DefaultSupplierContext implements SupplierContext, Cloneable {

    private static final Logger LOG = LoggerFactory.getLogger( DefaultSupplierContext.class );

    private final SupplierAssistant supplierAssistant;

    private final EnvironmentAssistant environmentAssistant;

    private final ConversionAssistant conversionAssistant;

    private final Object originalValue;

    private Object currentValue;

    private Map<Class<?>, Object> convertedValues;

    private Map<Class<?>, Object> environment;

    private Map<String, Object> attributes;

    /**
     * Copies the values from the provided context, with the same original value and current value.
     * All attributes are also shallow-copied into the new context.
     *
     * @param context The context to copy.
     */
    public DefaultSupplierContext( DefaultSupplierContext context ) {
        this( context, context.getOriginalValue() );
        withValue( context.getValue() );
    }

    /**
     * Copies the values from the provided context, with the supplied <code>value</code> which will be both the
     * original value and the current value for the context. All attributes will also be shallow-copied into the new
     * context.
     *
     * @param context The context to copy.
     * @param value   The new 'original' and current value.
     */
    public DefaultSupplierContext( DefaultSupplierContext context, Object value ) {
        this( context.getSupplierAssistant(), context.environmentAssistant, context.conversionAssistant, value );
        if ( context.attributes != null ) {
            this.attributes = new HashMap<String, Object>();
            for ( Map.Entry<String, Object> e : context.attributes.entrySet() ) {
                this.attributes.put( e.getKey(), e.getValue() );
            }
        }
    }

    public DefaultSupplierContext( SupplierAssistant supplierAssistant, EnvironmentAssistant environmentAssistant, ConversionAssistant conversionAssistant, Object value ) {
        this.supplierAssistant = supplierAssistant;
        this.environmentAssistant = environmentAssistant;
        this.conversionAssistant = conversionAssistant;
        originalValue = value;
        convertedValues = new HashMap<Class<?>, Object>( 10 );
        withValue( value );
    }

    @Override
    public Object getValue() {
        return currentValue;
    }

    @Override
    public <T> T getValueAs( Class<T> type ) {
        Object currentValue = getValue();
        if ( type.isInstance( currentValue ) )
            return type.cast( currentValue );

        if ( currentValue != null ) {
            Object convertedValue;
            if ( convertedValues.containsKey( type ) ) {
                convertedValue = convertedValues.get( type );
                if ( convertedValue != null )
                    return type.cast( convertedValue );
            } else {
                try {
                    convertedValue = conversionAssistant.convert( currentValue, type );
                    convertedValues.put( type, convertedValue );
                    if ( convertedValue != null ) {
                        return type.cast( convertedValue );
                    }
                } catch ( ConversionException e ) {
                    LOG.warn( "Conversion Exception: " + e.getMessage() );
                }
            }
        }

        return null;
    }

    @Override
    public boolean canGetValueAs( Class<?> type ) {
        Object currentValue = getValue();
        return currentValue != null && ( type.isInstance( currentValue ) || conversionAssistant.canConvert( currentValue, type ) );
    }

    @Override
    public void setValue( Object newValue ) {
        if ( currentValue != newValue ) {
            currentValue = newValue;
            convertedValues.clear();
        }
    }

    @Override
    public SupplierContext withValue( Object currentValue ) {
        setValue( currentValue );
        return this;
    }

    @Override
    public <T> T getEnvironmentValue( Class<T> type ) {
        Object value = null;
        if ( environment != null )
            value = environment.get( type );

        if ( value == null )
            value = environmentAssistant.getValue( type );

        if ( type.isInstance( value ) )
            return type.cast( value );

        return null;
    }

    @Override
    public <T> SupplierContext withEnvironmentValue( Class<T> type, T value ) {
        setEnvironmentValue( type, value );
        return this;
    }

    @Override
    public <T> void setEnvironmentValue( Class<T> type, T value ) {
        if ( environment == null )
            environment = new HashMap<Class<?>, Object>();
        environment.put( type, value );
    }

    @Override
    public Object getOriginalValue() {
        return originalValue;
    }

    @Override
    public <T> T getAttribute( String name, Class<T> type ) {
        if ( attributes != null ) {
            return cast( attributes.get( name ), type, null );
        }
        return null;
    }

    @Override
    public void setAttribute( String name, Object value ) {
        if ( attributes == null )
            attributes = new HashMap<String, Object>();
        attributes.put( name, value );
    }

    @Override
    public SupplierContext withAttribute( String name, Object value ) {
        setAttribute( name, value );
        return this;
    }

    @Override
    public SupplierAssistant getSupplierAssistant() {
        return supplierAssistant;
    }

    private <T> T cast( Object from, Class<T> to, T defaultValue ) {
        if ( to.isInstance( from ) )
            return to.cast( from );
        return defaultValue;
    }

    @Override
    public DefaultSupplierContext clone() throws CloneNotSupportedException {
        DefaultSupplierContext clone = (DefaultSupplierContext) super.clone();

        if ( attributes != null )
            clone.attributes = new HashMap<String, Object>( attributes );
        if ( environment != null )
            clone.environment = new HashMap<Class<?>, Object>( environment );

        return clone;
    }
}
