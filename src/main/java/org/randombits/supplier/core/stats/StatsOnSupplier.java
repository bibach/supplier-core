package org.randombits.supplier.core.stats;

import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import java.util.Iterator;

/**
 * This supplier will perform basic statistical analysis on a collection of
 * items
 *
 * @author David Peterson
 */
@SupplierPrefix(value = "stats-on", required = true)
@SupportedTypes(Iterator.class)
public class StatsOnSupplier extends AnnotatedSupplier {

    @SupplierKey("{keychain}")
    @API("1.0.0")
    public Stats getStats(@KeyValue Iterator iterator, @KeyParam("keychain") String keychain, @KeyContext SupplierContext context) throws SupplierException, CloneNotSupportedException {
        Stats stats = new Stats();
        while (iterator.hasNext()) {
            Object item = iterator.next();
            stats.process(context.getSupplierAssistant().findValue(context.clone().withValue(item), keychain), item);
        }
        return stats;
    }
}
