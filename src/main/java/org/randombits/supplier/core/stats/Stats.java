package org.randombits.supplier.core.stats;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Comparator;

@XmlRootElement
public class Stats {
    @SuppressWarnings("unchecked")
    private Comparator comparator;

    @XmlElement
    private double sum = 0;

    @XmlElement
    private int valueCount = 0;

    @XmlElement
    private int itemCount = 0;

    @XmlElement
    private Object maxValue;

    @XmlElement
    private Object maxItem;

    @XmlElement
    private Object minValue;

    @XmlElement
    private Object minItem;

    public Stats() {
        this(new StatsComparator());
    }

    @SuppressWarnings("unchecked")
    public Stats(Comparator comparator) {
        this.comparator = comparator;
    }

    public void process(Object value, Object item) {
        // Always increment the item count
        itemCount++;

        boolean isValue = isValue(value);

        if (isValue) {
            valueCount++;

            if (value instanceof Number) {
                Number number = (Number) value;
                sum += number.doubleValue();
            }

            if (comparator != null) {
                try {
                    if (maxValue == null || comparator.compare(maxValue, value) < 0) {
                        maxValue = value;
                        maxItem = item;
                    }

                    if (minValue == null || comparator.compare(minValue, value) > 0) {
                        minValue = value;
                        minItem = item;
                    }
                } catch (ClassCastException e) {
                    // We tried comparing incompable types. We can no longer say
                    // for certain what is the 'minValue' or 'maxValue' value.
                    comparator = null;
                    maxValue = null;
                    maxItem = null;
                    minValue = null;
                    minItem = null;
                }
            }
        }

    }

    private boolean isValue(Object value) {
        if (value instanceof String)
            return ((String) value).trim().length() > 0;
        else
            return value != null;
    }

    public double getSum() {
        return sum;
    }

    /**
     * @return the number of processed values which were not <code>null</code>
     *         or blank strings.
     */
    public int getValueCount() {
        return valueCount;
    }

    public int getEmptyCount() {
        return itemCount - valueCount;
    }

    public int getItemCount() {
        return itemCount;
    }

    public double getValueAverage() {
        return (valueCount != 0) ? sum / valueCount : 0d;
    }

    public double getItemAverage() {
        int total = getItemCount();
        return (total != 0) ? sum / total : 0d;
    }

    public Object getMaxValue() {
        return maxValue != this ? maxValue : null;
    }

    public Object getMaxItem() {
        return maxItem;
    }

    public Object getMinValue() {
        return minValue != this ? minValue : null;
    }

    public Object getMinItem() {
        return minItem;
    }
}
