package org.randombits.supplier.core.stats;

import java.util.Comparator;

public class StatsComparator implements Comparator<Object> {

    @SuppressWarnings("unchecked")
    public int compare(Object o1, Object o2) {
        if (o1 instanceof Number && o2 instanceof Number) {
            return Double.compare(((Number) o1).doubleValue(), ((Number) o2).doubleValue());
        } else if (o1 instanceof Comparable<?> && o1.getClass().isInstance(o2)) {
            return ((Comparable<Object>) o1).compareTo(o2);
        } else {
            throw new ClassCastException("Incomparable classes: " + o1.getClass().getName() + "; " + o2.getClass());
        }
    }

}
