package org.randombits.supplier.core;

/**
 * Thrown if there is an error while parsing a {@link KeyPattern}.
 */
public class KeyParseException extends RuntimeException {

    public KeyParseException() {
    }

    public KeyParseException( String s ) {
        super( s );
    }

    public KeyParseException( String s, Throwable throwable ) {
        super( s, throwable );
    }

    public KeyParseException( Throwable throwable ) {
        super( throwable );
    }
}
