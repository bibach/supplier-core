package org.randombits.supplier.core.general;

import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import java.util.Map;

@SupplierPrefix("entry")
@SupportedTypes(Map.Entry.class)
@API("1.0.0")
public class MapEntrySupplier extends AnnotatedSupplier {

    @SupplierKey("value")
    @API("1.0.0")
    public Object getValue(@KeyValue Map.Entry entry) {
        return entry.getValue();
    }

    @SupplierKey("key")
    @API("1.0.0")
    public Object getKey(@KeyValue Map.Entry entry) {
        return entry.getKey();
    }
}
