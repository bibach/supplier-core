package org.randombits.supplier.core.general;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class represents an individual match from a {@link Matcher}, to aid supplier in handling matches.
 */
public class Match {
    private final MatchResult matchResult;

    private final Matcher matcher;

    private boolean matches = true;

	private int index;

    public Match( MatchResult matchResult, int index) {
    	this.index = index;
        this.matchResult = matchResult;
        this.matcher = ( matchResult instanceof Matcher ) ? ( Matcher ) matchResult : null;
    }

    public Match(Matcher matcher) {
		this(matcher, 1);
	}

	public MatchResult getMatchResult() {
        return matchResult;
    }

    public Matcher getMatcher() {
        return matcher;
    }

    /**
     * Checks if the 'matches' method is required. If so, and the 'matcher' is
     * not null, calls 'reset()' then 'matches()'.
     */
    private void findMatch() {
        if (matcher != null ) {
            matcher.reset();
            for (int i = 0; i < index; i++) {
				matches = matcher.find();
			}
        }
    }

    public boolean matches() {
        findMatch();
        return matches;
    }

    public String replaceAll( String replacement ) {
        if (matches()) {
            return matcher.replaceAll( replacement );
        } else {
            return null;
        }
    }

    public String replaceFirst( String replacement ) {
        if (matches()) {
            return matcher.replaceFirst( replacement );
        } else {
            return null;
        }
    }

    public String group() {
        if ( matches() )
            return matchResult.group();
        return null;
    }

    public Integer start() {
        if ( matches() )
            return matchResult.start();
        return null;
    }

    public Integer end() {
        if ( matches() )
            return matchResult.end();
        return null;
    }

    public int groupCount() {
    	if ( matches() )
        return matchResult.groupCount();
    	return 0;
    }

    public Pattern pattern() {
        if ( matcher != null )
            return matcher.pattern();
        return null;
    }

    public String replaceWith( String with ) {
        if ( matcher != null ) {
            findMatch();
            StringBuffer sb = new StringBuffer();
            matcher.appendReplacement( sb, with );
            return sb.toString();
        }
        return null;
    }

    public String group( int index ) {
        if ( matches() ) {
            return matchResult.group( index );
        }
        return null;
    }
}
