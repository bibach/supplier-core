package org.randombits.supplier.core.general;

import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import java.util.List;
import java.util.regex.Pattern;

/**
 * This supplier provides access to the 'Matcher' regular expression class.
 */
@SupplierPrefix("match")
@SupportedTypes(Match.class)
@API("1.0.0")
public class MatchSupplier extends AnnotatedSupplier {

    private static final Pattern NUMBER_PATTERN = Pattern.compile("[0-9]+");

    @SupplierKey("value")
    @API("1.0.0")
    public String getValue(@KeyValue Match match) {
        try {
            return match.group();
        } catch (IllegalStateException e) {
            // Just return null.
            return null;
        }
    }

    @SupplierKey("start")
    @API("1.0.0")
    public Integer getStart(@KeyValue Match match) {
        try {
            return match.start();
        } catch (IllegalStateException e) {
            // Just return null.
            return null;
        }
    }

    @SupplierKey("end")
    @API("1.0.0")
    public Integer getEnd(@KeyValue Match match) {
        try {
            return match.end();
        } catch (IllegalStateException e) {
            // Just return null.
            return null;
        }
    }

    @SupplierKey("groups")
    @API("1.0.0")
    public List<String> getGroups(@KeyValue Match match) {
        try {
            List<String> groups = new java.util.ArrayList<String>(match.groupCount());
            for (int i = 1; i <= match.groupCount(); i++)
                groups.add(match.group(i));
            return groups;
        } catch (IllegalStateException e) {
            // Just return null.
            return null;
        }
    }

    @SupplierKey("group count")
    @KeyWeight(200)
    @API("1.0.0")
    public Integer getGroupCount(@KeyValue Match match) {
        try {
            return match.groupCount();
        } catch (IllegalStateException e) {
            // Just return null.
            return null;
        }
    }

    @SupplierKey("group {index}")
    @KeyWeight(100)
    @API("1.0.0")
    public String getGroupNumber(@KeyValue Match match, @KeyParam("index") String indexString) {
        try {
            if (NUMBER_PATTERN.matcher(indexString).matches()) {
                int index = Integer.parseInt(indexString);
                if (index <= match.groupCount())
                    return match.group(index);
            }
        } catch (IllegalStateException e) {
            // Just return null.
        }

        return null;

    }

    @SupplierKey("pattern")
    @API("1.0.0")
    public String getPattern(@KeyValue Match match) {
        return match.pattern().pattern();
    }

    @SupplierKey("replace with {replacement}")
    @API("1.0.0")
    public String replaceWith(@KeyValue Match match, @KeyParam("replacement") String replacement) {
        return match.replaceWith(replacement);
    }

    @SupplierKey("replace all with {replacement}")
    @API("1.0.0")
    public String replaceAllWith(@KeyValue Match match, @KeyParam("replacement") String replacement) {
        return match.replaceAll(replacement);
    }

    @SupplierKey("replace first with {replacement}")
    @API("1.0.0")
    public String replaceFirstWith(@KeyValue Match match, @KeyParam("replacement") String replacement) {
        return match.replaceFirst(replacement);
    }

}
