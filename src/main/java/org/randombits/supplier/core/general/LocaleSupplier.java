package org.randombits.supplier.core.general;

import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import java.util.Locale;

/**
 * Provides a supplier for {@link Locale} instances.
 */
@SupplierPrefix("locale")
@SupportedTypes(Locale.class)
public class LocaleSupplier extends AnnotatedSupplier {

    private Locale getUserLocale(SupplierContext context) {
        return context.getEnvironmentValue(Locale.class);
    }

    @SupplierKey("id")
    @API("1.0.0")
    public String getId(@KeyValue Locale locale) {
        return locale.toString();
    }

    @SupplierKey("language id")
    @API("1.0.0")
    public String getLanguageId(@KeyValue Locale locale) {
        return locale.getLanguage();
    }

    @SupplierKey("country id")
    @API("1.0.0")
    public String getCountryId(@KeyValue Locale locale) {
        return locale.getCountry();
    }

    @SupplierKey("variant id")
    @API("1.0.0")
    public String getVariantId(@KeyValue Locale locale) {
        return locale.getVariant();
    }

    @SupplierKey("language")
    @API("1.0.0")
    public String getLanguage(@KeyValue Locale locale, @KeyContext SupplierContext ctx) {
        return locale.getDisplayLanguage(getUserLocale(ctx));
    }

    @SupplierKey("country")
    @API("1.0.0")
    public String getCountry(@KeyValue Locale locale, @KeyContext SupplierContext ctx) {
        return locale.getDisplayCountry(getUserLocale(ctx));
    }

    @SupplierKey("variant")
    @API("1.0.0")
    public String getVariant(@KeyValue Locale locale, @KeyContext SupplierContext ctx) {
        return locale.getDisplayVariant(getUserLocale(ctx));
    }
}
