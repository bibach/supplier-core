package org.randombits.supplier.core;

/**
 * Transforms text from one form into another, and back.
 */
public interface Encoder {

    /**
     * Decodes the value into 'normal' text.
     *
     * @param value The encoded value.
     * @return the 'normal' text.
     */
    String decode( String value );

    /**
     * Encodes the value into the target text encoding.
     *
     * @param value The 'normal' value.
     * @return The encoded text.
     */
    String encode( String value );
}
