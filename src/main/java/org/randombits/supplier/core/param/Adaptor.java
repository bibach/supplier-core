package org.randombits.supplier.core.param;

/**
 * Adapts the object to the target type, if possible.
 */
public interface Adaptor<T> {
    Class<T> getTargetType();
    
    T adapt( Object value );
}
