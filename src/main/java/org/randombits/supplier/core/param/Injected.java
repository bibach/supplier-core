package org.randombits.supplier.core.param;

import java.lang.annotation.*;

/**
 * Marks a method as being injected.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Injected {

    Class<? extends Adaptor> adaptor() default Adaptor.class;
}
