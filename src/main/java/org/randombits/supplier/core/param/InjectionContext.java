package org.randombits.supplier.core.param;

import java.lang.annotation.*;

/**
 * This interface is used by the {@link InjectionInterceptor} to retrieve the desired
 * object that should be the context when performing an injection.
 * {@link org.randombits.support.core.param.ParameterSource}
 * contexts which wish to support injecting based on a specific value initialise their
 * should initialise an instance of this interface in the {@link org.randombits.support.core.param.ParameterContext}.
 */

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InjectionContext {

    /**
     * The type of class to access from the {@link org.randombits.support.core.param.ParameterContext}
     * to use as the root value for injection.
     *
     * @return The class type.
     */
    Class<?> value();
}
