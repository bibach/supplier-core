package org.randombits.supplier.core.param;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import org.randombits.supplier.core.SupplierAssistant;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.impl.InjectionAdaptorModuleDescriptor;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.MethodContext;
import org.randombits.support.core.param.ParameterInterceptor;
import org.springframework.beans.factory.DisposableBean;

import java.util.HashMap;
import java.util.Map;

/**
 * Intercepts methods in {@link org.randombits.support.core.param.Parameters} instances
 * that are marked with [@link Injected} and injects the input value before continuing. If the
 * result is an object of the target type for the method, it will be returned, otherwise it is
 * converted to a string and set as the new input value.
 */
public class InjectionInterceptor implements ParameterInterceptor, DisposableBean {

    private final SupplierAssistant supplierAssistant;

    private final PluginModuleTracker<Adaptor<?>, InjectionAdaptorModuleDescriptor> adaptorTracker;

    private final Map<Class<? extends Adaptor<?>>, Adaptor<?>> adaptors;

    public InjectionInterceptor( SupplierAssistant supplierAssistant, PluginAccessor pluginAccessor, PluginEventManager pluginEventManager ) {
        this.supplierAssistant = supplierAssistant;

        adaptors = new HashMap<Class<? extends Adaptor<?>>, Adaptor<?>>();

        adaptorTracker = new DefaultPluginModuleTracker<Adaptor<?>, InjectionAdaptorModuleDescriptor>( pluginAccessor, pluginEventManager, InjectionAdaptorModuleDescriptor.class,
                new PluginModuleTracker.Customizer<Adaptor<?>, InjectionAdaptorModuleDescriptor>() {
                    @Override
                    public InjectionAdaptorModuleDescriptor adding( InjectionAdaptorModuleDescriptor moduleDescriptor ) {
                        registerAdaptor( moduleDescriptor.getModule() );
                        return moduleDescriptor;
                    }

                    @Override
                    public void removed( InjectionAdaptorModuleDescriptor moduleDescriptor ) {
                        unregisterAdaptor( moduleDescriptor.getModule() );
                    }
                } );
    }

    private void unregisterAdaptor( Adaptor<?> adaptor ) {
        adaptors.remove( adaptor.getClass() );
    }

    private void registerAdaptor( Adaptor<?> adaptor ) {
        adaptors.put( (Class<? extends Adaptor<?>>) adaptor.getClass(), adaptor );
    }

    @Override
    public <T> Result intercept( MethodContext<T> context ) throws InterpretationException {
        Injected injected = context.getMethod().getAnnotation( Injected.class );
        if ( injected != null ) {
            try {
                Object injectionContext = findInjectionContext( context );
                Object value = supplierAssistant.injectValues( injectionContext, context.getInput() );

                if ( value != null ) {
                    T adapted = adapt( value, injected, context );
                    if ( adapted != null ) {
                        context.setReturnValue( adapted );
                        return Result.HALT;
                    } else {
                        // Set set the input to the string value of the injected value
                        context.setInput( value.toString() );
                    }
                } else {
                    context.setReturnValue( null );
                    return Result.HALT;
                }
            } catch ( SupplierException e ) {
                throw new InterpretationException( e.getMessage(), e );
            }
        }
        return Result.CONTINUE;
    }

    private <T> Object findInjectionContext( MethodContext<T> context ) {
        InjectionContext attr = context.getMethod().getAnnotation( InjectionContext.class );
        if ( attr == null ) {
            attr = findInjectionContextAnnotation( context.getMethod().getDeclaringClass() );
        }

        if ( attr != null ) {
            return context.getParameterContext().get( attr.value(), null );
        }

        return null;
    }

    private InjectionContext findInjectionContextAnnotation( Class<?> declaringClass ) {
        InjectionContext ann = declaringClass.getAnnotation( InjectionContext.class );
        if ( ann == null ) {
            for ( Class<?> i : declaringClass.getInterfaces() ) {
                ann = findInjectionContextAnnotation( i );
                if ( ann != null )
                    break;
            }
        }
        return ann;
    }

    private <T> T adapt( Object value, Injected injected, MethodContext<T> context ) throws InterpretationException {
        Class<T> returnType = context.getReturnType();

        if ( returnType.isInstance( value ) ) {
            return returnType.cast( value );
        }

        if ( injected.adaptor() != Adaptor.class ) {
            Adaptor<?> adaptor = adaptors.get( injected.adaptor() );
            if ( adaptor == null )
                throw new InterpretationException( "The adaptor must be registered via a <injection-adaptor> module in the plugin of origin: " + adaptor.getClass().getName() );

            if ( returnType.isAssignableFrom( adaptor.getTargetType() ) ) {
                return returnType.cast( adaptor.adapt( value ) );
            }

            throw new InterpretationException( "The provided Adaptor class does not return a compatible type for this method: " + context.getMethod() );
        }
        return null;
    }

    @Override
    public void destroy() throws Exception {
        adaptors.clear();
        adaptorTracker.close();
    }
}
