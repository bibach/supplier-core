package org.randombits.supplier.core.impl;

import java.util.Iterator;

/**
 * Represents a chain of {@link Key}s.
 */
class KeyChain implements Iterator<Key> {

    /**
     * The delimiter used to separate the prefix from the key value.
     */
    char PREFIX_DELIM = ':';

    private final Iterator<String> keys;

    public KeyChain( Iterator<String> keys ) {
        this.keys = keys;
    }

    @Override
    public boolean hasNext() {
        return keys.hasNext();
    }

    @Override
    public Key next() {
        String prefix = null;
        String name = keys.next();
        int prefixIndex = name.indexOf( PREFIX_DELIM );
        if ( prefixIndex > -1 ) {
            prefix = prefixIndex > 0 ? name.substring( 0, prefixIndex ) : null;
            name = name.substring( prefixIndex + 1 );
        }
        return new Key( prefix, name );
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
