package org.randombits.supplier.core.impl;

import com.atlassian.plugin.Plugin;
import org.randombits.supplier.core.Supplier;
import org.randombits.supplier.core.SupplierDetails;
import org.randombits.supplier.core.SupplierModuleDescriptor;
import org.randombits.utils.lang.API;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Provides more extended information about the suppliers installed in the system.
 */
@XmlRootElement
@API("1.0.0")
public class DefaultSupplierDetails implements SupplierDetails {
    private Supplier supplier;

    @XmlElement
    private String supplierClass;

    private Plugin plugin;

    @XmlElement
    private String pluginKey;

    @XmlElement
    private String moduleKey;

    @XmlElement
    private String name;

    @XmlElement
    private String description;

    public DefaultSupplierDetails(SupplierModuleDescriptor descriptor) {
        supplier = descriptor.getModule();
        supplierClass = supplier.getClass().getName();

        plugin = descriptor.getPlugin();
        pluginKey = descriptor.getPluginKey();
        moduleKey = descriptor.getKey();

        name = descriptor.getName();
        description = descriptor.getDescription();
    }

    @Override
    public String getSupplierClass() {
        return supplierClass;
    }

    @Override
    public String getPluginKey() {
        return pluginKey;
    }

    @Override
    public String getModuleKey() {
        return moduleKey;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Supplier getSupplier() {
        return supplier;
    }

    @Override
    public Plugin getPlugin() {
        return plugin;
    }
}
