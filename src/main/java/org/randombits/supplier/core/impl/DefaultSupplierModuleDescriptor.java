package org.randombits.supplier.core.impl;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.NotNull;
import org.randombits.supplier.core.Supplier;
import org.randombits.supplier.core.SupplierModuleDescriptor;
import org.dom4j.Element;

/**
 * Implementation of {@link org.randombits.supplier.core.SupplierModuleDescriptor}
 */
public class DefaultSupplierModuleDescriptor extends AbstractModuleDescriptor<Supplier> implements SupplierModuleDescriptor {

    private Supplier supplier;

    public DefaultSupplierModuleDescriptor( ModuleFactory moduleFactory ) {
        super( moduleFactory );
    }

    @Override
    public void init( @NotNull final Plugin plugin, @NotNull Element element ) throws PluginParseException {
        super.init( plugin, element );
        supplier = null;
    }

    @Override
    public Supplier getModule() {
        if ( supplier == null )
            supplier = moduleFactory.createModule( moduleClassName, this );
        return supplier;
    }
}
