/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.core.impl;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;

import org.apache.commons.lang.StringEscapeUtils;
import org.randombits.supplier.core.*;
import org.randombits.support.core.convert.ConversionAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.utils.lang.API;
import org.springframework.beans.factory.DisposableBean;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provides methods for accessing supplier information.
 *
 * @author David Peterson
 */
public class DefaultSupplierAssistant implements SupplierAssistant, DisposableBean {

    private static final Encoder NOOP_ENCODER = new Encoder() {

        @Override
        public String decode( String value ) {
            return value;
        }

        @Override
        public String encode( String value ) {
            return value;
        }
    };

    private static final String DEFAULT_SEPARATOR = ", ";

    private static final Pattern VALUE_PATTERN = Pattern.compile( "\\%((?:[^\\%]|\\%\\%)+)\\%" );

    private static final Pattern INJECTION_PATTERN = Pattern
            .compile( "\\G((?:[^\\%]|\\%\\%)+|\\%(?:[^\\%]|\\%\\%)+\\%)" );

    private static final String SELF_KEY = "@self";

    private final PluginModuleTracker<Supplier, SupplierModuleDescriptor> supplierTracker;

    private final EnvironmentAssistant environmentAssistant;

    private final ConversionAssistant conversionAssistant;

    private Map<String, Set<Supplier>> suppliersByPrefix = new HashMap<String, Set<Supplier>>();

    public DefaultSupplierAssistant( PluginAccessor pluginAccessor, PluginEventManager pluginEventManager, EnvironmentAssistant environmentAssistant, ConversionAssistant conversionAssistant ) {
        this.environmentAssistant = environmentAssistant;
        this.conversionAssistant = conversionAssistant;
        supplierTracker = new DefaultPluginModuleTracker<Supplier, SupplierModuleDescriptor>( pluginAccessor, pluginEventManager, SupplierModuleDescriptor.class,
                new PluginModuleTracker.Customizer<Supplier, SupplierModuleDescriptor>() {
                    @Override
                    public SupplierModuleDescriptor adding( SupplierModuleDescriptor descriptor ) {
                        cacheSupplier( descriptor.getModule() );
                        return descriptor;
                    }

                    @Override
                    public void removed( SupplierModuleDescriptor descriptor ) {
                        removeSupplier( descriptor.getModule() );
                    }
                }
        );
    }

    private void removeSupplier( Supplier supplier ) {
        if ( supplier != null ) {
            Set<String> prefixes = supplier.getPrefixes();

            if ( prefixes == null || prefixes.size() == 0 ) {
                removeSupplierPrefix( null, supplier );
            } else {
                if ( !supplier.isPrefixRequired() )
                    removeSupplierPrefix( null, supplier );

                for ( String prefix : supplier.getPrefixes() ) {
                    removeSupplierPrefix( prefix, supplier );
                }
            }
        }
    }

    private void removeSupplierPrefix( String prefix, Supplier supplier ) {
        Set<Supplier> suppliers = getSuppliers( prefix );
        if ( suppliers != null ) {
            suppliers.remove( supplier );
            if ( suppliers.size() == 0 )
                suppliersByPrefix.remove( prefix );
        }
    }

    private void cacheSupplier( Supplier supplier ) {
        if ( supplier != null ) {
            Set<String> prefixes = supplier.getPrefixes();

            // It's a 'null' prefix supplier.
            if ( prefixes == null || prefixes.size() == 0 ) {
                addSupplierPrefix( null, supplier );
            } else {
                if ( !supplier.isPrefixRequired() )
                    addSupplierPrefix( null, supplier );

                for ( String prefix : prefixes ) {
                    addSupplierPrefix( prefix, supplier );
                }
            }
        }
    }

    private void addSupplierPrefix( String prefix, Supplier supplier ) {
        Set<Supplier> suppliers = getSuppliers( prefix );
        if ( suppliers == null ) {
            suppliers = new HashSet<Supplier>( 5 );
            suppliersByPrefix.put( prefix, suppliers );
        }
        suppliers.add( supplier );
    }

    /**
     * @return The current set of suppliers.
     */
    @Override
    public Iterable<Supplier> getSuppliers() {
        return supplierTracker.getModules();
    }

    @Override
    public Iterable<SupplierDetails> getSupplierDetails() {
        Iterable<SupplierModuleDescriptor> moduleDescs = supplierTracker.getModuleDescriptors();
        ArrayList<SupplierDetails> detailsList = new ArrayList<SupplierDetails>();
        for ( SupplierModuleDescriptor moduleDescriptor : moduleDescs ) {
            detailsList.add( new DefaultSupplierDetails( moduleDescriptor ) );
        }
        return detailsList;
    }

    @Override
    public Iterable<SupplierDetails> getSupplierDetailsForPrefix( String prefix ) {
        // TODO: Make this more generic so we can search for suppliers
        Iterable<SupplierModuleDescriptor> moduleDescs = supplierTracker.getModuleDescriptors();
        ArrayList<SupplierDetails> detailsList = new ArrayList<SupplierDetails>();
        for ( SupplierModuleDescriptor moduleDescriptor : moduleDescs ) {
            if ( moduleDescriptor.getModule().getPrefixes().contains( prefix ) )
                detailsList.add( new DefaultSupplierDetails( moduleDescriptor ) );
        }
        return detailsList;
    }

    @Override
    public SupplierContext createContext( Object value ) {
        return new DefaultSupplierContext( this, environmentAssistant, conversionAssistant, value );
    }

    /**
     * @see org.randombits.supplier.core.SupplierAssistant#injectValues(Object, String)
     */
    @Override
    @API("1.0.0")
    public Object injectValues( Object value, String text ) throws SupplierException {
        return injectValues( createContext( value ), text );
    }

    @Override
    @API("1.0.0")
    public Object injectValues( Object value, String text, Encoder encoder ) throws SupplierException {
        return injectValues( createContext( value ), text, encoder );
    }

    public Object injectValues( SupplierContext context, String text ) throws SupplierException {
        return injectValues( context, text, null );
    }

    @Override
    @API
    public Object injectValues( SupplierContext context, String text, Encoder encoder ) throws SupplierException {
        encoder = encoder == null ? NOOP_ENCODER : encoder;

        if ( VALUE_PATTERN.matcher( text ).matches() ) {
            return findValue( context, unwrap( text, encoder ) );
        } else {
            Matcher matcher = INJECTION_PATTERN.matcher( text );
            StringBuilder out = new StringBuilder();
            int maxIndex = 0;

            Object currentValue = context.getValue();

            while ( matcher.find() ) {
                maxIndex = matcher.end();
                String token = matcher.group();
                if ( token.charAt( 0 ) == ESCAPE_DELIM ) {
                    token = unwrap( token, encoder );
               
                    Object value = findValue( context.withValue( currentValue ), token.replaceAll("&gt;", ">") );
                    appendValue( out, value, encoder );
                } else {
                    out.append( unescape( token ) );
                }
            }

            if ( maxIndex != text.length() )
                throw new SupplierException( "Unexpected value at " + maxIndex + ": " + text.substring( maxIndex ) );

            return out.toString();
        }
    }

    @Override
    public Object findValue( Object value, String keyChain ) throws SupplierException {
        return findValue( createContext( value ), keyChain );
    }

    /**
     * Attempts to find a value given the specified context and key. String
     * values are <b>not</b> rendered as wiki text.
     *
     * @param context  The context object.
     * @param keyChain The key list, separated by '>' characters.
     * @return the found value, or <code>defaultValue</code> if none could be
     *         found.
     * @throws SupplierException if there is a problem finding the value.
     */
    @Override
    public Object findValue( SupplierContext context, String keyChain ) throws SupplierException {
        return findProperty( context, keyChain, null );
    }

    @Override
    public <T> T findProperty( Object value, String keyChain, Supplier.Property<T> property ) throws SupplierException {
        return findProperty( createContext( value ), keyChain, property );
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T findProperty( SupplierContext context, String keyChain, Supplier.Property<T> property )
            throws SupplierException {
        return (T) processKeyChain( context, keyChain, property );
    }

    private Object processKeyChain( SupplierContext context, String keyChain, Supplier.Property<?> property ) throws SupplierException {
        Object returnValue = null;

        Object currentValue = context.getValue();

        if ( keyChain != null ) {
            KeyChain keys = KeyChainParser.parse( keyChain );
            returnValue = processNextKey( context, keys, property );
        }

        if ( returnValue == null && property != null ) {
            returnValue = getPropertyValue( context.withValue( currentValue ), property );
        }

        // Reset the current value.
        context.setValue( currentValue );

        return returnValue;
    }

    private Object processNextKey( SupplierContext context, KeyChain keys, Supplier.Property<?> property ) throws SupplierException {
        Key key = keys.next();

        Object value = getKeyValue( context, key );

        if ( value != null ) {
            if ( keys.hasNext() ) {
                // Still more items on the keychain - get the next value.
                Object nextValue = processNextKey( context.withValue( value ), keys, property );
                // We return the next value if it's been returned, or if there is no other property we're trying to dig up.
                if ( nextValue != null || property == null )
                    return nextValue;
            }

            // Check if we should return a property - properties always trump regular values, but only if the nextValue
            // was returned as null.
            if ( property != null )
                value = getPropertyValue( context.withValue( value ), property );
        }
        return value;


    }

    private Object getKeyValue( SupplierContext context, Key key ) throws SupplierException {
        // TODO: This purely exists for efficiency. Perhaps a better way is to provide a 'SelfSupplier' and make sure it's at the top of the list?
        if ( key.getPrefix() == null && SELF_KEY.equals( key.getName() ) )
            return context.getValue();

        Iterable<Supplier> suppliers = getSuppliers( key.getPrefix() );

        if ( suppliers != null ) {
            Object something;


            for ( Supplier supplier : suppliers ) {

                if ( supportsContext( context, supplier ) ) {

                    something = processSupplierKeys( supplier, context, key.getName() );

                    if ( something != null )
                        return something;
                }
            }
        }

        return null;
    }

    private Set<Supplier> getSuppliers( String prefix ) {
        return suppliersByPrefix.get( prefix );
    }

    private <T> T getPropertyValue( SupplierContext context, Supplier.Property<T> property ) throws SupplierException {
        Iterable<Supplier> suppliers = getSuppliers();

        if ( suppliers != null ) {
            T something;

            for ( Supplier supplier : suppliers ) {
                if ( supportsContext( context, supplier ) ) {
                    something = property.get( supplier, context );
                    if ( something != null )
                        return something;
                }
            }
        }

        return null;
    }

    private Object processSupplierKeys( Supplier supplier, SupplierContext context, String key ) throws SupplierException {
        Object result;
        for ( KeyHandler keyHandler : supplier.getKeyDetails() ) {
            result = keyHandler.process( context, key );
            if ( result != null )
                return result;
        }
        return null;
    }

    private boolean supportsContext( SupplierContext context, Supplier supplier ) {
        Set<Class<?>> supportedTypes = supplier.getSupportedTypes();
        // A null list means all types are supported, including null.
        if ( context.getValue() == null && supplier.isNullAllowed() || supportedTypes == null )
            return true;

        for ( Class<?> supportedType : supportedTypes ) {
            if ( context.canGetValueAs( supportedType ) )
                return true;
        }

        return false;
    }

    @Override
    public void destroy() throws Exception {
        supplierTracker.close();
    }

    private String unwrap( String token, Encoder encoder ) {
        String keyChain = token.substring( 1, token.length() - 1 );
        return unescape( encoder.decode( keyChain ) );
    }

    private String unescape( String token ) {
        return token.replaceAll( "\\%\\%", "%" );
    }

    private void appendValue( StringBuilder out, Object value, Encoder encoder ) {
        if ( value == null )
            return;

        if ( value.getClass().isArray() ) {
            Object[] values = (Object[]) value;
            for ( int i = 0; i < values.length; i++ ) {
                if ( i != 0 )
                    out.append( DEFAULT_SEPARATOR );
                appendValue( out, values[i], encoder );
            }
        } else if ( value instanceof Collection<?> ) {
            appendCollection( out, (Collection<?>) value, encoder );
        } else if ( value instanceof Iterator<?> ) {
            appendIterator( out, (Iterator<?>) value, encoder );
        } else {
            out.append( encoder.encode( value.toString() ) );
        }
    }

    private void appendCollection( StringBuilder out, Collection<?> collection, Encoder encoder ) {
        appendIterator( out, collection.iterator(), encoder );
    }

    private void appendIterator( StringBuilder out, Iterator<?> iterator, Encoder encoder ) {
        while ( iterator.hasNext() ) {
            Object value = iterator.next();
            appendValue( out, value, encoder );
            if ( iterator.hasNext() )
                out.append( DEFAULT_SEPARATOR );
        }
    }

}
