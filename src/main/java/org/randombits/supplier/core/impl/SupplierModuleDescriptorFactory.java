package org.randombits.supplier.core.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import org.randombits.supplier.core.SupplierModuleDescriptor;

/**
 * A factory for {@link org.randombits.supplier.core.SupplierModuleDescriptor}s.
 */
public class SupplierModuleDescriptorFactory extends SingleModuleDescriptorFactory<SupplierModuleDescriptor> {

    private final ModuleFactory moduleFactory;

    public SupplierModuleDescriptorFactory( HostContainer hostContainer, ModuleFactory moduleFactory ) {
        super(hostContainer, "supplier", SupplierModuleDescriptor.class);
        this.moduleFactory = moduleFactory;
    }

    @Override
    public ModuleDescriptor getModuleDescriptor(String type) throws PluginParseException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        return hasModuleDescriptor(type) ? new DefaultSupplierModuleDescriptor( moduleFactory ) : null;
    }
}