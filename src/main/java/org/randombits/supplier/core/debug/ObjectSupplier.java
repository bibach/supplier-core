package org.randombits.supplier.core.debug;

import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

/**
 * A supplier that provides basic information about Object instances, such as 'class', 'classname', 'as string',
 * and 'hash code'.
 */
@API("1.0.0")
@SupplierPrefix(value = "object", required = true)
@SupportedTypes(Object.class)
public class ObjectSupplier extends AnnotatedSupplier {

    @SupplierKey("hash code")
    @API("1.0.0")
    public static int getHashCode( @KeyValue Object value ) {
        return value.hashCode();
    }

    @SupplierKey({"as text", "as string"})
    @API("1.0.0")
    public static String asText( @KeyValue Object value ) {
        return value.toString();
    }

    @SupplierKey({"classname", "class name"})
    @API("1.0.0")
    public static String getClassName( @KeyValue Object value ) {
        return value.getClass().getName();
    }

    @SupplierKey("class")
    @API("1.0.0")
    public static Class<?> getClass( @KeyValue Object value ) {
        return value.getClass();
    }


}
