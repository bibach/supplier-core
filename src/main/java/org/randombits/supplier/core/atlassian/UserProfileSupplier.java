package org.randombits.supplier.core.atlassian;

import com.atlassian.sal.api.user.UserProfile;
import org.randombits.supplier.core.DisplayableSupplier;
import org.randombits.supplier.core.LinkableSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

/**
 * Represents a {@link UserProfile}.
 */
@SupplierPrefix({"user-profile"})
@SupportedTypes(UserProfile.class)
@API("1.0.0")
public class UserProfileSupplier extends AnnotatedSupplier implements LinkableSupplier, DisplayableSupplier {

    @SupplierKey({"username", "name"})
    @API("1.0.0")
    public String getUsername( @KeyValue UserProfile user ) {
        return user.getUsername();
    }

    @SupplierKey("full name")
    @API("1.0.0")
    public String getFullName( @KeyValue UserProfile user ) {
        return user.getFullName();
    }

    @SupplierKey("picture")
    @API("1.0.0")
    public String getPictureUri( @KeyValue UserProfile user ) {
    	if(user.getProfilePictureUri()!=null)
    		return user.getProfilePictureUri().toString();
    	return null;
    }

    @SupplierKey("email")
    @API("1.0.0")
    public String getEmail( @KeyValue UserProfile user ) {
        return user.getEmail();
    }

    @SupplierKey("url")
    private String getUrl( @KeyValue UserProfile userProfile ) {
        return userProfile.getProfilePageUri().toString();
    }

    @Override
    public String getLink( SupplierContext context ) throws SupplierException {
        UserProfile userProfile = context.getValueAs( UserProfile.class );
        return userProfile == null ? null : getUrl( userProfile );
    }

    @Override
    public String getDisplayText( SupplierContext context ) throws SupplierException {
        UserProfile userProfile = context.getValueAs( UserProfile.class );
        if ( userProfile == null )
            return null;
        if ( isBlank( userProfile.getFullName() ) )
            return userProfile.getUsername();
        return userProfile.getFullName();
    }

    private boolean isBlank( String value ) {
        return value == null || value.trim().isEmpty();
    }
}
