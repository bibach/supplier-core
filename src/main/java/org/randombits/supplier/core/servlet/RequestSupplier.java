package org.randombits.supplier.core.servlet;

import org.apache.commons.collections.IteratorUtils;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.annotate.AnnotatedSupplier;
import org.randombits.supplier.core.annotate.KeyContext;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.utils.lang.API;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

@SupplierPrefix(value = "request", required = true)
@API("1.0.0")
public class RequestSupplier extends AnnotatedSupplier {

    @SupplierKey("is secure")
    @API("1.0.0")
    public Boolean isSecure(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.isSecure();
    }

    @SupplierKey("server port")
    @API("1.0.0")
    public Integer getServerPort(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.getServerPort();
    }

    @SupplierKey("server name")
    @API("1.0.0")
    public String getServerName(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.getServerName();
    }

    @SupplierKey("scheme")
    @API("1.0.0")
    public String getScheme(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.getScheme();
    }

    @SupplierKey({"request url", "request uri"})
    @API("1.0.0")
    public String getRequestURL(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.getRequestURI();
    }

    @SupplierKey("remote host")
    @API("1.0.0")
    public String getRemoteHost(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.getRemoteHost();
    }

    @SupplierKey("remote address")
    @API("1.0.0")
    public String getRemoteAddress(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.getRemoteAddr();
    }

    @SupplierKey("query")
    @API("1.0.0")
    public String getQuery(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.getQueryString();
    }

    @SupplierKey("protocol")
    @API("1.0.0")
    public String getProtocol(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.getProtocol();
    }

    @SupplierKey("context path")
    @API("1.0.0")
    public String getContextPath(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        return req.getContextPath();
    }

    private HttpServletRequest getRequest(SupplierContext context) {
        return context.getEnvironmentValue(HttpServletRequest.class);
    }

    @SupplierKey("parameters")
    @API("1.0.0")
    public Map getParameters(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req != null)
            return req.getParameterMap();
        return null;
    }

    @SuppressWarnings({"unchecked"})
    @SupplierKey("headers")
    @API("1.0.0")
    public Map<String, Iterator<String>> getHeadersMap(@KeyContext SupplierContext context) {
        HttpServletRequest req = getRequest(context);
        if (req == null)
            return null;

        Map<String, Iterator<String>> headers = new java.util.HashMap<String, Iterator<String>>();

        Enumeration<String> names = req.getHeaderNames();
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            Enumeration<String> values = req.getHeaders(name);
            headers.put(name, values == null ? null : IteratorUtils.asIterator(values));
        }

        return headers;
    }
}
