package it.org.randombits.supplier.core;

import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import it.com.servicerocket.randombits.AddOnTest;
import it.com.servicerocket.randombits.pageobject.AddOnOSGIPage;
import org.junit.Test;

/**
 * @author HengHwa
 * @since 1.0.7.20141007
 */
public class SupplierCoreTest extends AbstractWebDriverTest {
    @Test public void testSupplierCorePluginIsInstalled() throws Exception {
        assert new AddOnTest().isInstalled(
            product.login(User.ADMIN, AddOnOSGIPage.class),
            "RB Supplier - Core"
        );
    }
}
