package org.randombits.supplier.core.impl;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.google.common.collect.Sets;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.api.Invocation;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.internal.ExpectationBuilder;
import org.jmock.lib.action.CustomAction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.randombits.supplier.core.*;
import org.randombits.support.core.convert.ConversionAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;

import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(JMock.class)
public class DefaultSupplierAssistantTest {

    private static final String PRE = "Pre ";

    private static final String POST = " Post";

    private static final String SIMPLE_PREFIX = "simple";

    private static final String SIMPLE_KEY = "value";

    private static final String SIMPLE_KEYCHAIN = SIMPLE_PREFIX + ":" + SIMPLE_KEY;

    private DefaultSupplierAssistant supplierAssistant;

    private Supplier supplier;

    private SupplierModuleDescriptor supplierModuleDescriptor;

    private PluginAccessor pluginAccessor;

    private PluginEventManager pluginEventManager;

    private DefaultPluginModuleTracker tracker;

    private SupplierContext supplierContext;

    private Mockery context = new JUnit4Mockery();

    private KeyHandler keyHandler;

    private EnvironmentAssistant environmentAssistant;

    private <T> T mock( Class<T> type ) {
        return context.mock( type );
    }

    private void checking( ExpectationBuilder expectations ) {
        context.checking( expectations );
    }

    @Before
    public void setUp() throws Exception {

        environmentAssistant = mock( EnvironmentAssistant.class );
        ConversionAssistant conversionAssistant = mock( ConversionAssistant.class );

        supplier = mock( Supplier.class );
        supplierModuleDescriptor = mock( SupplierModuleDescriptor.class );
        supplierContext = mock( SupplierContext.class );
        keyHandler = mock( KeyHandler.class );

        pluginAccessor = mock( PluginAccessor.class );
        pluginEventManager = mock( PluginEventManager.class );

        context.checking( new Expectations() {
            {
                // Allow registration of the DefaultPluginModuleTracker instance, and keep a reference for later.
                one( pluginEventManager ).register( with( any( DefaultPluginModuleTracker.class ) ) );
                will( new CustomAction( "get tracker" ) {
                    @Override
                    public Object invoke( Invocation invocation ) throws Throwable {
                        tracker = (DefaultPluginModuleTracker) invocation.getParameter( 0 );
                        return null;
                    }
                } );

                // Return an empty list.
                one( pluginAccessor ).getEnabledModuleDescriptorsByClass( SupplierModuleDescriptor.class );
                will( returnValue( Collections.EMPTY_LIST ) );

                // Always return our mock supplier.
                allowing( supplierModuleDescriptor ).getModule();
                will( returnValue( supplier ) );
            }
        } );

        supplierAssistant = new DefaultSupplierAssistant( pluginAccessor, pluginEventManager, environmentAssistant, conversionAssistant );
    }

    @After
    public void tearDown() throws Exception {

        supplier = null;
        pluginAccessor = null;
        pluginEventManager = null;
        supplierAssistant = null;
        environmentAssistant = null;
    }

    @Test
    public void testGetSuppliersEmpty() {
        Iterable<Supplier> suppliers = supplierAssistant.getSuppliers();
        assertNotNull( suppliers );
        assertFalse( suppliers.iterator().hasNext() );
    }

    private void addSupplier( Supplier supplier, String prefix, boolean prefixRequired, Class<?>... types ) {
        expectSupplierSupport( supplier, prefix, prefixRequired, types );
        assertNotNull( tracker );
        tracker.onPluginModuleEnabled( new PluginModuleEnabledEvent( supplierModuleDescriptor ) );
    }

    private void expectSupplierSupport( final Supplier supplier, final String prefix, final boolean prefixRequired, final Class<?>... supportedTypes ) {
        final Set<String> prefixes = Collections.singleton( prefix );
        final Set<Class<?>> types = Sets.newHashSet( supportedTypes );
        checking( new Expectations() {
            {
                allowing( supplier ).getPrefixes();
                will( returnValue( prefixes ) );

                allowing( supplier ).isPrefixRequired();
                will( returnValue( prefixRequired ) );

                allowing( supplier ).getSupportedTypes();
                will( returnValue( types ) );
            }
        } );
    }

    private void expectSupplierResult( final Supplier supplier, final SupplierContext supplierContext, final Object value, final String key, final Object result ) throws SupplierException {

        checking( new Expectations() {
            {
                allowing( supplierContext ).getValue();
                will( returnValue( value ) );

                allowing( supplierContext ).withValue( value );
                will( returnValue( supplierContext ) );
                
                allowing( supplierContext ).setValue( value );

                one( supplier ).getKeyDetails();
                will( returnValue( Collections.singleton( keyHandler ) ) );

                if ( key != null ) {
                    one( supplierContext ).canGetValueAs( key.getClass() );
                    will( returnValue( true ) );
                } else {
                    one( supplierContext ).canGetValueAs( null );
                    will( returnValue( true ) );
                }

                one( keyHandler ).process( supplierContext, key );
                will( returnValue( result ) );
            }
        } );
    }

    @Test
    public void testInjectValuesSimple() throws SupplierException {

        addSupplier( supplier, SIMPLE_PREFIX, false, String.class );

        final String result = "result";


        expectSupplierResult( supplier, supplierContext, "value", SIMPLE_KEY, result );

        Object processed = supplierAssistant.injectValues( supplierContext, PRE + "%" + SIMPLE_KEYCHAIN + "%" + POST );
        assertEquals( PRE + result + POST, processed );
    }

    @Test
    public void testInjectValuesOriginal() throws SupplierException {

        addSupplier( supplier, SIMPLE_PREFIX, false, String.class );

        final Number result = 2;

        expectSupplierResult( supplier, supplierContext, "value", SIMPLE_KEY, result );

        Object processed = supplierAssistant.injectValues( supplierContext, "%" + SIMPLE_KEYCHAIN + "%" );
        assertSame( result, processed );
    }

    @Test
    public void testInjectValuesTwoSpaces() throws SupplierException {
        addSupplier( supplier, SIMPLE_PREFIX, false, String.class );

        final String result = "result";

        expectSupplierResult( supplier, supplierContext, "value", SIMPLE_KEY, result );
        expectSupplierResult( supplier, supplierContext, "value", SIMPLE_KEY, result );

        Object processed = supplierAssistant.injectValues( supplierContext, "%" + SIMPLE_KEYCHAIN + "%  %" + SIMPLE_KEYCHAIN + "%" );
        assertEquals( result + "  " + result, processed );
    }

    @Test
    public void testInjectValuesEscapedPercents() throws SupplierException {
        addSupplier( supplier, SIMPLE_PREFIX, false, String.class );

        final String result = "result";

        expectSupplierResult( supplier, supplierContext, "value", SIMPLE_KEY, result );
        expectSupplierResult( supplier, supplierContext, "value", SIMPLE_KEY, result );

        Object processed = supplierAssistant.injectValues( supplierContext, PRE + "%% %" + SIMPLE_KEYCHAIN + "% %% %" + SIMPLE_KEYCHAIN + "% %%" + POST );
        assertEquals( PRE + "% " + result + " % " + result + " %" + POST, processed );
    }

    @Test
    public void testInjectValuesArray() throws SupplierException {

        addSupplier( supplier, SIMPLE_PREFIX, false, String.class );

        final String[] result = new String[]{"result1", "result2"};

        expectSupplierResult( supplier, supplierContext, "value", SIMPLE_KEY, result );

        Object processed = supplierAssistant.injectValues( supplierContext, PRE + "%" + SIMPLE_KEYCHAIN + "%" + POST );
        assertEquals( PRE + "result1, result2" + POST, processed );
    }

    @Test
    public void testFindValueSimple() throws SupplierException {
        addSupplier( supplier, "test", false, String.class );

        final String test = "test";

        expectSupplierResult( supplier, supplierContext, "value", "key", test );

        try {
            Object result = supplierAssistant.findValue( supplierContext, "test:key" );
            assertSame( test, result );
        } catch ( SupplierException e ) {
            fail( "Unexpected SupplierException: " + e.getLocalizedMessage() );
        }
    }

    @Test
    public void testFindValueChain() throws SupplierException {
        addSupplier( supplier, "test", false, String.class );

        final String key1 = "key1";
        final String key2 = "key2";

        expectSupplierResult( supplier, supplierContext, "value", "key1", key1 );
        expectSupplierResult( supplier, supplierContext, key1, "key2", key2 );

        try {
            Object result = supplierAssistant.findValue( supplierContext, "test:key1 > test:key2" );
            assertSame( key2, result );
        } catch ( SupplierException e ) {
            fail( "Unexpected ParameterParsingException: " + e.getLocalizedMessage() );
        }
    }
}
