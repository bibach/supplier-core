package org.randombits.supplier.core;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.HashMap;
import java.util.Map;

/**
 * KeyPattern Tester.
 *
 * @author David Peterson
 * @version 1.0
 * @since <pre>11/19/2011</pre>
 */
public class KeyPatternTest extends TestCase {

    public KeyPatternTest( String name ) {
        super( name );
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Method: getPattern()
     */
    public void testGetPattern() throws Exception {
        KeyPattern test = KeyPattern.parse( "test" );
        assertEquals( "test", test.getPattern() );
    }

    /**
     * Method: match(String keyValue)
     */
    public void testMatchSimple() throws Exception {
        KeyPattern test = KeyPattern.parse( "test" );

        assertEquals( "success", KeyPattern.Status.COMPLETE, test.match( "test" ) );
        assertEquals( "fail", KeyPattern.Status.UNMATCHED, test.match( "tset" ) );
        assertEquals( "partial", KeyPattern.Status.PARTIAL, test.match( "tes" ) );
        assertEquals( "excess", KeyPattern.Status.EXCESS, test.match( "tests" ) );
    }

    public void testMatchWithParam() throws Exception {
        KeyPattern test = KeyPattern.parse( "test <{param}>" );

        assertEquals( "success", KeyPattern.Status.COMPLETE, test.match( "test <value>" ) );
        assertEquals( "success", KeyPattern.Status.COMPLETE, test.match( "test <>" ) );
        assertEquals( "fail", KeyPattern.Status.UNMATCHED, test.match( "tset <value>" ) );
        assertEquals( "partial", KeyPattern.Status.PARTIAL, test.match( "tes" ) );
        assertEquals( "partial", KeyPattern.Status.PARTIAL, test.match( "test <value" ) );
        assertEquals( "excess", KeyPattern.Status.EXCESS, test.match( "test <value>s" ) );
    }

    private void doProcess( KeyPattern keyPattern, String keyValue, KeyPattern.Status status, String... params ) {
        Map<String, String> paramMap = new HashMap<String, String>();

        KeyPattern.Status statusResult = keyPattern.process( keyValue, paramMap );

        assertEquals( "status", status, statusResult );

        if ( params != null ) {
            if ( params.length / 2 != paramMap.size() )
                fail( "Expected to have " + paramMap.size() + " parameter values to test." );

            int i = 0;
            while ( i + 1 < params.length ) {
                assertEquals( "'" + params[i] + " parameter", params[i + 1], paramMap.get( params[i] ) );
                i += 2;
            }
        } else {
            if ( paramMap.size() > 0 )
                fail( "Expected parameter values to test." );
        }
    }

    /**
     * Method: process(String keyValue, Map<String, ? super String> params)
     */
    public void testProcessForKeyValueParams() throws Exception {
        KeyPattern test = KeyPattern.parse( "test {one}, {two}." );

        doProcess( test, "test 1, 2.", KeyPattern.Status.COMPLETE, "one", "1", "two", "2" );
        doProcess( test, "test 1, 2, 3.", KeyPattern.Status.COMPLETE, "one", "1", "two", "2, 3" );
        doProcess( test, "text 1, 2.", KeyPattern.Status.UNMATCHED );
        doProcess( test, "test 1,2.", KeyPattern.Status.PARTIAL );
        doProcess( test, "test 1", KeyPattern.Status.PARTIAL );
        doProcess( test, "test 1,", KeyPattern.Status.PARTIAL );
        doProcess( test, "test 1, 2", KeyPattern.Status.PARTIAL );
        doProcess( test, "test 1, .", KeyPattern.Status.COMPLETE, "one", "1", "two", "" );
        doProcess( test, "test 1, 2. 3.", KeyPattern.Status.EXCESS );
    }

    /**
     * Method: parse(String pattern)
     */
    public void testParse() throws Exception {
        KeyPattern.parse( "test" );
        KeyPattern.parse( "test {one}, {two}." );
        KeyPattern.parse( "{one}" );
        KeyPattern.parse( "{foo} {bar}" );

        try {
            KeyPattern.parse( "{one" );
            fail( "Expected parse exception for missing '}'." );
        } catch ( KeyParseException e ) {
        }

        try {
            KeyPattern.parse( "{one {two}" );
            fail( "Expected parse exception for missing '}' on '{one'" );
        } catch ( KeyParseException e ) {
        }

        try {
            KeyPattern.parse( "test } value" );
            fail( "Expected parse exception for missing '{'.");
        } catch ( KeyParseException e ) {
        }

        try {
            KeyPattern.parse( "{one}{two}" );
            fail( "Expected parse exception because there is no token boundary between the two parameters. ");
        } catch (KeyParseException e ) {
        }
    }

    public static Test suite() {
        return new TestSuite( KeyPatternTest.class );
    }
}
